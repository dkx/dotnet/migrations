using DKX.Migrations.Configurations;
using DKX.Migrations.Loaders;
using DKX.Migrations.Tests.Integration.Internal;

namespace DKX.Migrations.Tests.Integration.Files;

public sealed class RecursiveDirectoriesMigrationsLoaderTest
{
	[Fact]
	public void LoadMigrations()
	{
		// Arrange
		var root = Helpers.GetProjectDirectory();
		var loader = new RecursiveDirectoriesMigrationsLoader(
			"*.sql",
			new[]
			{
				new GroupConfiguration { Name = "struct", Directory = Path.Combine(root, "Migrations/structure") },
				new GroupConfiguration { Name = "data", Directory = Path.Combine(root, "Migrations/basic-data") },
			}
		);

		// Act
		var migrations = loader.LoadMigrations().ToArray();

		// Assert
		Assert.Equal(2, migrations.Length);
		Assert.Equal("struct", migrations[0].Group);
		Assert.Equal("01", migrations[0].Version);
		Assert.Equal("data", migrations[1].Group);
		Assert.Equal("02", migrations[1].Version);
	}
}
