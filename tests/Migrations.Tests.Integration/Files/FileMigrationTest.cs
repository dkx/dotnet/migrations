using DKX.Migrations.Migrations;
using DKX.Migrations.Tests.Integration.Internal;

namespace DKX.Migrations.Tests.Integration.Files;

public sealed class FileMigrationTest
{
	[Fact]
	public void GetContent()
	{
		// Arrange
		var migration = new FileMigration
		{
			Group = "structure",
			Version = "01",
			Path = Helpers.GetMigrationPath("structure/01/01.sql"),
		};

		// Act
		var content = migration.GetContent();

		// Assert
		Assert.Equal(
			"""
			-- # Migration Up
			SELECT 1;
			SELECT 2;

			-- # Migration Down

			""".TrimStart(),
			content
		);
	}
}
