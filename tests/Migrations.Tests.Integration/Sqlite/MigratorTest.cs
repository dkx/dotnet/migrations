using DKX.Migrations.Migrations;
using DKX.Migrations.Tests.Integration.Internal;

namespace DKX.Migrations.Tests.Integration.Sqlite;

public sealed class MigratorTest : SqliteTestCase
{
	[Fact]
	public void Migrate_CreateTable()
	{
		// Arrange
		var migrator = Platform.CreateMigrator(true, new IMigration[] { });

		// Act
		migrator.Migrate();
		migrator.Migrate();
		migrator.Migrate();

		// Assert
		Assert.True(TableExists("__dkx_migrations"));
	}

	[Fact]
	public void Migrate_NoMigrations()
	{
		// Arrange
		var migrator = Platform.CreateMigrator(true, new IMigration[] { });

		// Act
		migrator.Migrate();

		// Assert
		Assert.Equal(0, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM __dkx_migrations"));
	}

	[Fact]
	public void Migrate_FromNothing()
	{
		// Arrange
		var migrator = Platform.CreateMigrator(
			true,
			new IMigration[]
			{
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-19_05:35:50",
					Content = """
					CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL);
					INSERT INTO users (email) VALUES ('john@doe.com');
					""",
				},
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-20_05:35:50",
					Content = "INSERT INTO users (email) VALUES ('lord@voldemort.com')",
				},
			}
		);

		// Act
		migrator.Migrate();

		// Assert
		Assert.Equal(2, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM __dkx_migrations"));
		Assert.Equal(
			"2020-05-19_05:35:50",
			Connection.ExecuteScalar<string>("SELECT version FROM __dkx_migrations ORDER BY version LIMIT 1 OFFSET 0")
		);
		Assert.Equal(
			"2020-05-20_05:35:50",
			Connection.ExecuteScalar<string>("SELECT version FROM __dkx_migrations ORDER BY version LIMIT 1 OFFSET 1")
		);
		Assert.Equal(2, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM users"));
		Assert.Equal("john@doe.com", Connection.ExecuteScalar<string>("SELECT email FROM users ORDER BY id LIMIT 1 OFFSET 0"));
		Assert.Equal("lord@voldemort.com", Connection.ExecuteScalar<string>("SELECT email FROM users ORDER BY id LIMIT 1 OFFSET 1"));
	}

	[Fact]
	public void Migrate_FromNothing_DryRun()
	{
		// Arrange
		var migrator = Platform.CreateMigrator(
			true,
			new IMigration[]
			{
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-19_05:35:50",
					Content = """
					CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL);
					INSERT INTO users (email) VALUES ('john@doe.com');
					""",
				},
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-20_05:35:50",
					Content = "INSERT INTO users (email) VALUES ('lord@voldemort.com')",
				},
			}
		);

		// Act
		migrator.Migrate(new MigrateOptions { DryRun = true });

		// Assert
		Assert.Equal(0, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM __dkx_migrations"));
		Assert.False(TableExists("users"));
	}

	[Fact]
	public void Migrate_Incremental()
	{
		// Arrange
		Platform.TryCreateTableStorage(null, "__dkx_migrations", "id", "version", "executed_at");
		Platform.InsertVersionRecord(null, "__dkx_migrations", "version", "executed_at", "2020-05-19_05:35:50", DateTime.Now);
		Connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");
		Connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");

		var migrator = Platform.CreateMigrator(
			true,
			new IMigration[]
			{
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-19_05:35:50",
					Content = string.Empty,
				},
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-20_05:35:50",
					Content = "INSERT INTO users (email) VALUES ('lord@voldemort.com')",
				},
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-21_05:35:50",
					Content = "INSERT INTO users (email) VALUES ('harry@potter.com')",
				},
			}
		);

		// Act
		migrator.Migrate();

		// Assert
		Assert.Equal(3, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM __dkx_migrations"));
		Assert.Equal(
			"2020-05-20_05:35:50",
			Connection.ExecuteScalar<string>("SELECT version FROM __dkx_migrations ORDER BY version LIMIT 1 OFFSET 1")
		);
		Assert.Equal(
			"2020-05-21_05:35:50",
			Connection.ExecuteScalar<string>("SELECT version FROM __dkx_migrations ORDER BY version LIMIT 1 OFFSET 2")
		);
		Assert.Equal(3, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM users"));
		Assert.Equal("lord@voldemort.com", Connection.ExecuteScalar<string>("SELECT email FROM users ORDER BY id LIMIT 1 OFFSET 1"));
		Assert.Equal("harry@potter.com", Connection.ExecuteScalar<string>("SELECT email FROM users ORDER BY id LIMIT 1 OFFSET 2"));
	}

	[Fact]
	public void Migrate_AllOrNothing_True()
	{
		// Arrange
		var migrator = Platform.CreateMigrator(
			true,
			new IMigration[]
			{
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-19_05:35:50",
					Content = """
					CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL);
					INSERT INTO users (email) VALUES ('john@doe.com');
					""",
				},
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-20_05:35:50",
					Content = "LOREM IPSUM DOLOR SIT AMET",
				},
			}
		);

		// Act
		try
		{
			migrator.Migrate();
		}
		catch
		{
			// ignored
		}

		// Assert
		Assert.Equal(0, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM __dkx_migrations"));
		Assert.False(TableExists("users"));
	}

	[Fact]
	public void Migrate_AllOrNothing_False()
	{
		// Arrange
		var migrator = Platform.CreateMigrator(
			false,
			new IMigration[]
			{
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-19_05:35:50",
					Content = """
					CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL);
					INSERT INTO users (email) VALUES ('john@doe.com');
					""",
				},
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-20_05:35:50",
					Content = "LOREM IPSUM DOLOR SIT AMET",
				},
			}
		);

		// Act
		try
		{
			migrator.Migrate();
		}
		catch
		{
			// ignored
		}

		// Assert
		Assert.Equal(1, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM __dkx_migrations"));
		Assert.Equal("2020-05-19_05:35:50", Connection.ExecuteScalar<string>("SELECT version FROM __dkx_migrations"));
		Assert.True(TableExists("users"));
		Assert.Equal("john@doe.com", Connection.ExecuteScalar<string>("SELECT email FROM users"));
	}

	[Fact]
	public void FindLastExecutedMigration_Throws_MigrationNotExists()
	{
		// Arrange
		Platform.TryCreateTableStorage(null, "__dkx_migrations", "id", "version", "executed_at");
		Platform.InsertVersionRecord(null, "__dkx_migrations", "version", "executed_at", "2020-05-19_05:35:50", DateTime.Now);

		var migrator = Platform.CreateMigrator(true, new IMigration[] { });

		// Act
		Action fn = () => migrator.Migrate();

		// Assert
		var e = Assert.Throws<Exception>(fn);
		Assert.Equal("Migration 2020-05-19_05:35:50 does not exists", e.Message);
	}

	[Fact]
	public void Migrate_SkipMigrations_FromNothing()
	{
		// Arrange
		var migrator = Platform.CreateMigrator(
			true,
			new IMigration[]
			{
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-19_05:35:50",
					Content = "CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)",
				},
				new StaticMigration
				{
					Group = "basic-data",
					Version = "2020-05-20_05:35:50",
					Content = "INSERT INTO users (email) VALUES ('john@doe.com')",
				},
				new StaticMigration
				{
					Group = "dummy-data",
					Version = "2020-05-21_05:35:50",
					Content = "INSERT INTO users (email) VALUES ('lord@voldemort.com')",
				},
			}
		);

		// Act
		migrator.Migrate(new MigrateOptions { Groups = new[] { "structure", "basic-data" } });

		// Assert
		Assert.Equal(2, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM __dkx_migrations"));
		Assert.Equal(
			"2020-05-19_05:35:50",
			Connection.ExecuteScalar<string>("SELECT version FROM __dkx_migrations ORDER BY version LIMIT 1 OFFSET 0")
		);
		Assert.Equal(
			"2020-05-20_05:35:50",
			Connection.ExecuteScalar<string>("SELECT version FROM __dkx_migrations ORDER BY version LIMIT 1 OFFSET 1")
		);
		Assert.Equal(1, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM users"));
		Assert.Equal("john@doe.com", Connection.ExecuteScalar<string>("SELECT email FROM users ORDER BY id LIMIT 1 OFFSET 0"));
	}

	[Fact]
	public void Migrate_ShouldMigrateToLatest_WhenGivenMigrationsWithUpAndDownParts()
	{
		// Arrange
		var migrator = Platform.CreateMigrator(
			true,
			new IMigration[]
			{
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-19_05:35:50",
					Content = """
					-- # Migration Up
					CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL);
					-- # Migration Down
					DROP TABLE users;
					""",
				},
				new StaticMigration
				{
					Group = "basic-data",
					Version = "2020-05-20_05:35:50",
					Content = """
					-- # Migration Up
					INSERT INTO users (email) VALUES ('john@doe.com');
					-- # Migration Down
					DELETE FROM users WHERE email = 'john@doe.com';
					""",
				},
				new StaticMigration
				{
					Group = "basic-data",
					Version = "2020-05-21_05:35:50",
					Content = """
					-- # Migration Up
					INSERT INTO users (email) VALUES ('lord@voldemort.com');
					-- # Migration Down
					DELETE FROM users WHERE email = 'lord@voldemort.com';
					""",
				},
			}
		);

		// Act
		migrator.Migrate();

		// Assert
		Assert.Equivalent(
			new[] { "2020-05-19_05:35:50", "2020-05-20_05:35:50", "2020-05-21_05:35:50" },
			Connection.ExecuteScalarList<string>("SELECT version FROM __dkx_migrations")
		);
		Assert.True(TableExists("users"));
		Assert.Equivalent(new[] { "john@doe.com", "lord@voldemort.com" }, Connection.ExecuteScalarList<string>("SELECT email FROM users"));
	}

	[Fact]
	public void Migrate_ShouldMigrateToInitialState_WhenGivenZero()
	{
		// Arrange
		var migrator = Platform.CreateMigrator(
			true,
			new IMigration[]
			{
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-19_05:35:50",
					Content = """
					-- # Migration Up
					CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL);
					-- # Migration Down
					DROP TABLE users;
					""",
				},
				new StaticMigration
				{
					Group = "basic-data",
					Version = "2020-05-20_05:35:50",
					Content = """
					-- # Migration Up
					INSERT INTO users (email) VALUES ('john@doe.com');
					-- # Migration Down
					DELETE FROM users WHERE email = 'john@doe.com';
					""",
				},
				new StaticMigration
				{
					Group = "basic-data",
					Version = "2020-05-21_05:35:50",
					Content = """
					-- # Migration Up
					INSERT INTO users (email) VALUES ('lord@voldemort.com');
					-- # Migration Down
					DELETE FROM users WHERE email = 'lord@voldemort.com';
					""",
				},
			}
		);

		migrator.Migrate();

		// Act
		migrator.Migrate(new MigrateOptions { ToVersion = "0" });

		// Assert
		Assert.Equal(0, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM __dkx_migrations"));
		Assert.False(TableExists("users"));
	}

	[Fact]
	public void Migrate_ShouldMigrateToPreviousState_WhenGivenPreviousVersion()
	{
		// Arrange
		var migrator = Platform.CreateMigrator(
			true,
			new IMigration[]
			{
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-19_05:35:50",
					Content = """
					-- # Migration Up
					CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL);
					-- # Migration Down
					DROP TABLE users;
					""",
				},
				new StaticMigration
				{
					Group = "basic-data",
					Version = "2020-05-20_05:35:50",
					Content = """
					-- # Migration Up
					INSERT INTO users (email) VALUES ('john@doe.com');
					-- # Migration Down
					DELETE FROM users WHERE email = 'john@doe.com';
					""",
				},
				new StaticMigration
				{
					Group = "basic-data",
					Version = "2020-05-21_05:35:50",
					Content = """
					-- # Migration Up
					INSERT INTO users (email) VALUES ('lord@voldemort.com');
					-- # Migration Down
					DELETE FROM users WHERE email = 'lord@voldemort.com';
					""",
				},
			}
		);

		migrator.Migrate();

		// Act
		migrator.Migrate(new MigrateOptions { ToVersion = "2020-05-19_05:35:50" });

		// Assert
		Assert.Equivalent(new[] { "2020-05-19_05:35:50" }, Connection.ExecuteScalarList<string>("SELECT version FROM __dkx_migrations"));
		Assert.True(TableExists("users"));
		Assert.Equal(0, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM users"));
	}

	[Fact]
	public void Migrate_ShouldThrow_WhenMigratingDownUsingLegacyFormat()
	{
		// Arrange
		var migrator = Platform.CreateMigrator(
			true,
			new IMigration[]
			{
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-19_05:35:50",
					Content = """
					CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL);
					""",
				},
				new StaticMigration
				{
					Group = "basic-data",
					Version = "2020-05-20_05:35:50",
					Content = """
					INSERT INTO users (email) VALUES ('john@doe.com');
					""",
				},
				new StaticMigration
				{
					Group = "basic-data",
					Version = "2020-05-21_05:35:50",
					Content = """
					INSERT INTO users (email) VALUES ('lord@voldemort.com');
					""",
				},
			}
		);

		migrator.Migrate();

		// Act
		Action fn = () => migrator.Migrate(new MigrateOptions { ToVersion = "2020-05-19_05:35:50" });

		// Assert
		var e = Assert.Throws<Exception>(fn);
		Assert.Equal("Down migration is not defined", e.Message);
		Assert.Equal(3, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM __dkx_migrations"));
	}
}
