using DKX.Migrations.Tests.Integration.Internal;

namespace DKX.Migrations.Tests.Integration.Sqlite;

public sealed class RunnerTest : SqliteTestCase
{
	[Fact]
	public void Run()
	{
		// Arrange
		var (runner, _) = CreateRunner();

		// Act
		runner.Migrate(new MigrateOptions { Yes = true });

		// Assert
		Assert.Equal(2, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM __dkx_migrations"));
		Assert.Equal("01", Connection.ExecuteScalar<string>("SELECT version FROM __dkx_migrations ORDER BY id LIMIT 1 OFFSET 0"));
		Assert.Equal("02", Connection.ExecuteScalar<string>("SELECT version FROM __dkx_migrations ORDER BY id LIMIT 1 OFFSET 1"));
	}

	[Fact]
	public void Run_SpecificGroups()
	{
		// Arrange
		var (runner, _) = CreateRunner();

		// Act
		runner.Migrate(new MigrateOptions { Yes = true, Groups = new[] { "structure" } });

		// Assert
		Assert.Equal(1, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM __dkx_migrations"));
		Assert.Equal("01", Connection.ExecuteScalar<string>("SELECT version FROM __dkx_migrations ORDER BY id LIMIT 1 OFFSET 0"));
	}

	[Fact]
	public void Run_SpecificGroups_WithDisabled()
	{
		// Arrange
		var (runner, _) = CreateRunner();

		// Act
		runner.Migrate(new MigrateOptions { Yes = true, Groups = new[] { "structure", "dummy-data" } });

		// Assert
		Assert.Equal(2, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM __dkx_migrations"));
		Assert.Equal("01", Connection.ExecuteScalar<string>("SELECT version FROM __dkx_migrations ORDER BY id LIMIT 1 OFFSET 0"));
		Assert.Equal("03", Connection.ExecuteScalar<string>("SELECT version FROM __dkx_migrations ORDER BY id LIMIT 1 OFFSET 1"));
	}

	[Fact]
	public void Run_ShouldMigrateDown_WhenEmptyDownMigration()
	{
		// Arrange
		var (runner, output) = CreateRunner();
		runner.Migrate(new MigrateOptions { Yes = true });
		output.Clear();

		// Act
		runner.Migrate(new MigrateOptions { Yes = true, ToVersion = "0" });

		// Assert
		Assert.Equal(0, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM __dkx_migrations"));
		Assert.Equal(
			new List<string>
			{
				"Migrating down to structure/01 from basic-data/02",
				"",
				"++ migrating basic-data/02",
				"  -> WARNING! No queries to execute",
				"++ migrated (took 42ms, 0 rows affected)",
				"",
				"++ migrating structure/01",
				"  -> WARNING! No queries to execute",
				"++ migrated (took 42ms, 0 rows affected)",
				"",
				"------------------------",
				"",
				"++ finished in 42ms",
				"++ 2 migrations executed",
			},
			output.Storage
		);
	}
}
