using DKX.Migrations.Tests.Integration.Internal;

namespace DKX.Migrations.Tests.Integration.Postgres;

public sealed class PlatformTest : PostgresTestCase
{
	[Fact]
	public void TryCreateTableStorage()
	{
		// Act
		Platform.TryCreateTableStorage(null, "__dkx_migrations", "id", "version", "executed_at");
		Platform.TryCreateTableStorage(null, "__dkx_migrations", "id", "version", "executed_at");
		Platform.TryCreateTableStorage(null, "__dkx_migrations", "id", "version", "executed_at");

		// Assert
		Assert.True(TableExists("__dkx_migrations"));
	}

	[Fact]
	public void TryCreateTableStorage_WithSchema()
	{
		// Act
		Platform.TryCreateTableStorage(null, "system.__dkx_migrations", "id", "version", "executed_at");
		Platform.TryCreateTableStorage(null, "system.__dkx_migrations", "id", "version", "executed_at");
		Platform.TryCreateTableStorage(null, "system.__dkx_migrations", "id", "version", "executed_at");

		// Assert
		Assert.True(TableExists("__dkx_migrations", "system"));
	}

	[Fact]
	public void GetLastExecutedVersion_Null()
	{
		// Arrange
		Platform.TryCreateTableStorage(null, "__dkx_migrations", "id", "version", "executed_at");

		// Act
		var latest = Platform.GetLastExecutedVersion(null, "__dkx_migrations", "id", "version");

		// Assert
		Assert.Null(latest);
	}

	[Fact]
	public void GetLastExecutedVersion()
	{
		// Arrange
		Platform.TryCreateTableStorage(null, "__dkx_migrations", "id", "version", "executed_at");
		Platform.InsertVersionRecord(
			null,
			"__dkx_migrations",
			"version",
			"executed_at",
			"2020-05-19_05:35:50",
			DateTime.SpecifyKind(DateTime.Now.AddDays(-3), DateTimeKind.Utc)
		);
		Platform.InsertVersionRecord(
			null,
			"__dkx_migrations",
			"version",
			"executed_at",
			"2020-05-20_05:35:50",
			DateTime.SpecifyKind(DateTime.Now.AddDays(-2), DateTimeKind.Utc)
		);
		Platform.InsertVersionRecord(
			null,
			"__dkx_migrations",
			"version",
			"executed_at",
			"2020-05-21_05:35:50",
			DateTime.SpecifyKind(DateTime.Now.AddDays(-1), DateTimeKind.Utc)
		);

		// Act
		var latest = Platform.GetLastExecutedVersion(null, "__dkx_migrations", "id", "version");

		// Assert
		Assert.Equal("2020-05-21_05:35:50", latest);
	}
}
