using DKX.Migrations.Migrations;
using DKX.Migrations.Tests.Integration.Internal;

namespace DKX.Migrations.Tests.Integration.MySql;

public sealed class MigratorTest : MySqlTestCase
{
	[Fact]
	public void Migrate_CreateTable()
	{
		// Arrange
		var migrator = Platform.CreateMigrator(true, new IMigration[] { });

		// Act
		migrator.Migrate();
		migrator.Migrate();
		migrator.Migrate();

		// Assert
		Assert.True(TableExists("__dkx_migrations"));
	}

	[Fact]
	public void Migrate_NoMigrations()
	{
		// Arrange
		var migrator = Platform.CreateMigrator(true, new IMigration[] { });

		// Act
		migrator.Migrate();

		// Assert
		Assert.Equal(0, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM __dkx_migrations"));
	}

	[Fact]
	public void Migrate_FromNothing()
	{
		// Arrange
		var migrator = Platform.CreateMigrator(
			true,
			new IMigration[]
			{
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-19_05:35:50",
					Content = """
					CREATE TABLE users (id INTEGER NOT NULL AUTO_INCREMENT, email VARCHAR(255) NOT NULL, PRIMARY KEY (id)) ENGINE=InnoDB;
					INSERT INTO users (email) VALUES ('john@doe.com');
					""",
				},
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-20_05:35:50",
					Content = "INSERT INTO users (email) VALUES ('lord@voldemort.com')",
				},
			}
		);

		// Act
		migrator.Migrate();

		// Assert
		Assert.Equal(2, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM __dkx_migrations"));
		Assert.Equal(
			"2020-05-19_05:35:50",
			Connection.ExecuteScalar<string>("SELECT version FROM __dkx_migrations ORDER BY version LIMIT 1 OFFSET 0")
		);
		Assert.Equal(
			"2020-05-20_05:35:50",
			Connection.ExecuteScalar<string>("SELECT version FROM __dkx_migrations ORDER BY version LIMIT 1 OFFSET 1")
		);
		Assert.Equal(2, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM users"));
		Assert.Equal("john@doe.com", Connection.ExecuteScalar<string>("SELECT email FROM users ORDER BY id LIMIT 1 OFFSET 0"));
		Assert.Equal("lord@voldemort.com", Connection.ExecuteScalar<string>("SELECT email FROM users ORDER BY id LIMIT 1 OFFSET 1"));
	}

	[Fact]
	public void Migrate_FromNothing_DryRun()
	{
		// Arrange
		var migrator = Platform.CreateMigrator(
			true,
			new IMigration[]
			{
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-19_05:35:50",
					Content = """
					CREATE TABLE users (id INTEGER NOT NULL AUTO_INCREMENT, email VARCHAR(255) NOT NULL, PRIMARY KEY (id)) ENGINE=InnoDB;
					INSERT INTO users (email) VALUES ('john@doe.com');
					""",
				},
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-20_05:35:50",
					Content = "INSERT INTO users (email) VALUES ('lord@voldemort.com')",
				},
			}
		);

		// Act
		migrator.Migrate(new MigrateOptions { DryRun = true });

		// Assert
		Assert.Equal(0, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM __dkx_migrations"));
		Assert.False(TableExists("users"));
	}

	[Fact]
	public void Migrate_Incremental()
	{
		// Arrange
		Platform.TryCreateTableStorage(null, "__dkx_migrations", "id", "version", "executed_at");
		Platform.InsertVersionRecord(null, "__dkx_migrations", "version", "executed_at", "2020-05-19_05:35:50", DateTime.Now);
		Connection.Execute(
			"CREATE TABLE users (id INTEGER NOT NULL AUTO_INCREMENT, email VARCHAR(255) NOT NULL, PRIMARY KEY (id)) ENGINE=InnoDB"
		);
		Connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");

		var migrator = Platform.CreateMigrator(
			true,
			new IMigration[]
			{
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-19_05:35:50",
					Content = string.Empty,
				},
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-20_05:35:50",
					Content = "INSERT INTO users (email) VALUES ('lord@voldemort.com')",
				},
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-21_05:35:50",
					Content = "INSERT INTO users (email) VALUES ('harry@potter.com')",
				},
			}
		);

		// Act
		migrator.Migrate();

		// Assert
		Assert.Equal(3, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM __dkx_migrations"));
		Assert.Equal(
			"2020-05-20_05:35:50",
			Connection.ExecuteScalar<string>("SELECT version FROM __dkx_migrations ORDER BY version LIMIT 1 OFFSET 1")
		);
		Assert.Equal(
			"2020-05-21_05:35:50",
			Connection.ExecuteScalar<string>("SELECT version FROM __dkx_migrations ORDER BY version LIMIT 1 OFFSET 2")
		);
		Assert.Equal(3, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM users"));
		Assert.Equal("lord@voldemort.com", Connection.ExecuteScalar<string>("SELECT email FROM users ORDER BY id LIMIT 1 OFFSET 1"));
		Assert.Equal("harry@potter.com", Connection.ExecuteScalar<string>("SELECT email FROM users ORDER BY id LIMIT 1 OFFSET 2"));
	}

	[Fact]
	public void Migrate_AllOrNothing_True()
	{
		// Arrange
		Platform.TryCreateTableStorage(null, "__dkx_migrations", "id", "version", "executed_at");
		Platform.InsertVersionRecord(null, "__dkx_migrations", "version", "executed_at", "2020-05-18_05:35:50", DateTime.Now);
		Connection.Execute(
			"CREATE TABLE users (id INTEGER NOT NULL AUTO_INCREMENT, email VARCHAR(255) NOT NULL, PRIMARY KEY (id)) ENGINE=InnoDB"
		);

		var migrator = Platform.CreateMigrator(
			true,
			new IMigration[]
			{
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-19_05:35:50",
					Content = "INSERT INTO users (email) VALUES ('john@doe.com')",
				},
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-20_05:35:50",
					Content = "LOREM IPSUM DOLOR SIT AMET",
				},
			}
		);

		// Act
		try
		{
			migrator.Migrate();
		}
		catch
		{
			// ignored
		}

		// Assert
		Assert.Equal(1, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM __dkx_migrations"));
		Assert.Equal(0, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM users"));
	}

	[Fact]
	public void Migrate_AllOrNothing_False()
	{
		// Arrange
		var migrator = Platform.CreateMigrator(
			false,
			new IMigration[]
			{
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-19_05:35:50",
					Content = """
					CREATE TABLE users (id INTEGER NOT NULL AUTO_INCREMENT, email VARCHAR(255) NOT NULL, PRIMARY KEY (id)) ENGINE=InnoDB;
					INSERT INTO users (email) VALUES ('john@doe.com');
					""",
				},
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-20_05:35:50",
					Content = "LOREM IPSUM DOLOR SIT AMET",
				},
			}
		);

		// Act
		try
		{
			migrator.Migrate();
		}
		catch
		{
			// ignored
		}

		// Assert
		Assert.Equal(1, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM __dkx_migrations"));
		Assert.Equal("2020-05-19_05:35:50", Connection.ExecuteScalar<string>("SELECT version FROM __dkx_migrations"));
		Assert.True(TableExists("users"));
		Assert.Equal("john@doe.com", Connection.ExecuteScalar<string>("SELECT email FROM users"));
	}

	[Fact]
	public void FindLastExecutedMigration_Throws_MigrationNotExists()
	{
		// Arrange
		Platform.TryCreateTableStorage(null, "__dkx_migrations", "id", "version", "executed_at");
		Platform.InsertVersionRecord(null, "__dkx_migrations", "version", "executed_at", "2020-05-19_05:35:50", DateTime.Now);

		var migrator = Platform.CreateMigrator(true, new IMigration[] { });

		// Act
		Action fn = () => migrator.Migrate();

		// Assert
		var e = Assert.Throws<Exception>(fn);
		Assert.Equal("Migration 2020-05-19_05:35:50 does not exists", e.Message);
	}

	[Fact]
	public void Migrate_SkipMigrations_FromNothing()
	{
		// Arrange
		var migrator = Platform.CreateMigrator(
			true,
			new IMigration[]
			{
				new StaticMigration
				{
					Group = "structure",
					Version = "2020-05-19_05:35:50",
					Content = "CREATE TABLE users (id INTEGER NOT NULL AUTO_INCREMENT, email TEXT NOT NULL, PRIMARY KEY (id))",
				},
				new StaticMigration
				{
					Group = "basic-data",
					Version = "2020-05-20_05:35:50",
					Content = "INSERT INTO users (email) VALUES ('john@doe.com')",
				},
				new StaticMigration
				{
					Group = "dummy-data",
					Version = "2020-05-21_05:35:50",
					Content = "INSERT INTO users (email) VALUES ('lord@voldemort.com')",
				},
			}
		);

		// Act
		migrator.Migrate(new MigrateOptions { Groups = new[] { "structure", "basic-data" } });

		// Assert
		Assert.Equal(2, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM __dkx_migrations"));
		Assert.Equal(
			"2020-05-19_05:35:50",
			Connection.ExecuteScalar<string>("SELECT version FROM __dkx_migrations ORDER BY version LIMIT 1 OFFSET 0")
		);
		Assert.Equal(
			"2020-05-20_05:35:50",
			Connection.ExecuteScalar<string>("SELECT version FROM __dkx_migrations ORDER BY version LIMIT 1 OFFSET 1")
		);
		Assert.Equal(1, Connection.ExecuteScalar<long>("SELECT COUNT(*) FROM users"));
		Assert.Equal("john@doe.com", Connection.ExecuteScalar<string>("SELECT email FROM users ORDER BY id LIMIT 1 OFFSET 0"));
	}
}
