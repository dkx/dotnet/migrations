using System.Reflection;

namespace DKX.Migrations.Tests.Integration.Internal;

public static class Helpers
{
	public static string GetProjectDirectory()
	{
		var exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)?.Replace('\\', '/');
		if (exePath is null)
		{
			throw new Exception();
		}

		var index = exePath.LastIndexOf("/bin/", StringComparison.Ordinal);
		if (index < 0)
		{
			throw new Exception();
		}

		return exePath.Substring(0, index);
	}

	public static string GetMigrationPath(string name)
	{
		return Path.Join(GetProjectDirectory(), "Migrations", name);
	}
}
