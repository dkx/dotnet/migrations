﻿using System.Data;
using DKX.Migrations.Platforms;
using Npgsql;
using Testcontainers.PostgreSql;

namespace DKX.Migrations.Tests.Integration.Internal;

public abstract class PostgresTestCase : IAsyncLifetime
{
	private readonly PostgreSqlContainer _container = new PostgreSqlBuilder().Build();

	private IDbConnection? _connection;

	private IPlatform? _platform;

	protected IDbConnection Connection => _connection ?? throw new InvalidOperationException("Connection is not initialized");

	protected IPlatform Platform => _platform ?? throw new InvalidOperationException("Platform is not initialized");

	public async Task InitializeAsync()
	{
		await _container.StartAsync();
		_connection = new NpgsqlConnection(_container.GetConnectionString());
		_platform = new PostgresPlatform(_connection);
	}

	public async Task DisposeAsync()
	{
		_connection?.Dispose();
		_platform?.Dispose();
		await _container.DisposeAsync();
	}

	protected bool TableExists(string name, string schema = "public")
	{
		return Connection.ExecuteScalar<bool>(
			$"SELECT EXISTS(SELECT * FROM information_schema.tables WHERE table_schema = '{schema}' AND table_name = '{name}')"
		);
	}
}
