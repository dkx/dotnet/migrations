using System.Data;
using DKX.Migrations.Configurations;
using DKX.Migrations.Output;
using DKX.Migrations.Platforms;
using DKX.Migrations.Watch;
using Microsoft.Data.Sqlite;

namespace DKX.Migrations.Tests.Integration.Internal;

public abstract class SqliteTestCase : IDisposable
{
	protected readonly IDbConnection Connection;

	protected readonly SqlitePlatform Platform;

	protected SqliteTestCase()
	{
		Connection = new SqliteConnection("Data Source=:memory:");
		Platform = new SqlitePlatform(Connection);
	}

	public void Dispose()
	{
		Connection.Dispose();
	}

	protected bool TableExists(string name)
	{
		var found = Connection.ExecuteScalar<string?>($"SELECT name FROM sqlite_master WHERE type = 'table' AND name = '{name}' LIMIT 1");
		return found == name;
	}

	protected (Runner Runner, StoredOutputWriter OutputWriter) CreateRunner()
	{
		var root = Helpers.GetProjectDirectory();

		var configuration = new Configuration
		{
			Driver = Driver.Sqlite,
			ConnectionString = string.Empty,
			Groups = new[]
			{
				new GroupConfiguration { Name = "structure", Directory = Path.Combine(root, "Migrations/structure") },
				new GroupConfiguration { Name = "basic-data", Directory = Path.Combine(root, "Migrations/basic-data") },
				new GroupConfiguration
				{
					Name = "dummy-data",
					Directory = Path.Combine(root, "Migrations/dummy-data"),
					Enabled = false,
				},
			},
		};

		var outputWriter = new StoredOutputWriter();

		return (new Runner(configuration, Platform, outputWriter, new StaticWatchFactory()), outputWriter);
	}
}
