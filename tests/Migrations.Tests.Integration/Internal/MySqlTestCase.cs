﻿using System.Data;
using DKX.Migrations.Platforms;
using MySql.Data.MySqlClient;
using Testcontainers.MySql;

namespace DKX.Migrations.Tests.Integration.Internal;

public abstract class MySqlTestCase : IAsyncLifetime
{
	private readonly MySqlContainer _container = new MySqlBuilder().Build();

	private IDbConnection? _connection;

	private IPlatform? _platform;

	protected IDbConnection Connection => _connection ?? throw new InvalidOperationException("Connection is not initialized");

	protected IPlatform Platform => _platform ?? throw new InvalidOperationException("Platform is not initialized");

	public async Task InitializeAsync()
	{
		await _container.StartAsync();
		_connection = new MySqlConnection(_container.GetConnectionString());
		_platform = new MysqlPlatform(_connection);
	}

	public async Task DisposeAsync()
	{
		_connection?.Dispose();
		_platform?.Dispose();
		await _container.DisposeAsync();
	}

	protected bool TableExists(string name)
	{
		var count = Connection.ExecuteScalar<long>(
			$"SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = '{Connection.Database}' AND table_name = '{name}'"
		);
		return count > 0;
	}
}
