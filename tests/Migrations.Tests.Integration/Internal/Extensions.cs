﻿using System.Data;
using DKX.Migrations.Configurations;
using DKX.Migrations.Loaders;
using DKX.Migrations.Migrations;
using DKX.Migrations.Output;
using DKX.Migrations.Parsing;
using DKX.Migrations.Platforms;
using DKX.Migrations.Watch;

namespace DKX.Migrations.Tests.Integration.Internal;

internal static class Extensions
{
	public static Migrator CreateMigrator(
		this IPlatform platform,
		bool allOrNothing,
		IReadOnlyCollection<IMigration> migrations,
		string? tableName = null
	)
	{
		var configuration = new Configuration
		{
			Driver = Driver.Sqlite,
			ConnectionString = string.Empty,
			AllOrNothing = allOrNothing,
		};

		if (tableName is not null)
		{
			configuration = configuration with { StorageTableName = tableName };
		}

		return new Migrator(
			platform,
			configuration,
			new StaticMigrationsLoader(migrations),
			new StoredOutputWriter(),
			new Parser(),
			new StaticWatchFactory()
		);
	}

	public static void Execute(this IDbConnection connection, string sql)
	{
		if (connection.State != ConnectionState.Open)
		{
			connection.Open();
		}

		using var command = connection.CreateCommand();
		command.CommandText = sql;
		command.ExecuteNonQuery();
	}

	public static TResult ExecuteScalar<TResult>(this IDbConnection connection, string sql)
	{
		if (connection.State != ConnectionState.Open)
		{
			connection.Open();
		}

		using var command = connection.CreateCommand();
		command.CommandText = sql;

		var result = command.ExecuteScalar();
		if (result is DBNull or null)
		{
			return default!;
		}

		if (result is TResult typedResult)
		{
			return typedResult;
		}

		throw new InvalidOperationException($"Invalid result type: {result.GetType()}");
	}

	public static IReadOnlyCollection<TResult> ExecuteScalarList<TResult>(this IDbConnection connection, string sql)
	{
		if (connection.State != ConnectionState.Open)
		{
			connection.Open();
		}

		using var command = connection.CreateCommand();
		command.CommandText = sql;

		var result = new List<TResult>();
		using var reader = command.ExecuteReader();

		while (reader.Read())
		{
			var value = reader[0];
			if (value is DBNull or null)
			{
				continue;
			}

			if (value is TResult typedValue)
			{
				result.Add(typedValue);
			}
			else
			{
				throw new InvalidOperationException($"Invalid result type: {value?.GetType()}");
			}
		}

		return result;
	}
}
