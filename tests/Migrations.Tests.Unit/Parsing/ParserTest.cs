﻿using Parser = DKX.Migrations.Parsing.Parser;

namespace DKX.Migrations.Tests.Unit.Parsing;

public sealed class ParserTest
{
	private readonly Parser _sut = new();

	[Fact]
	public void Parse_ShouldReturnSingleUpQuery_WhenUsingLegacySimpleFormat()
	{
		// Arrange
		const string content = "SELECT 1;";

		// Act
		var migration = _sut.Parse(content);

		// Assert
		Assert.Equivalent(new[] { "SELECT 1;" }, migration.Up);
		Assert.Null(migration.Down);
	}

	[Fact]
	public void Parse_ShouldReturnMultipleUpQueries_WhenUsingLegacySimpleFormat()
	{
		// Arrange
		const string content = """
			SELECT 1;
			SELECT 2;
			SELECT 3;
			""";

		// Act
		var migration = _sut.Parse(content);

		// Assert
		Assert.Equivalent(new[] { "SELECT 1;\nSELECT 2;\nSELECT 3;" }, migration.Up);
		Assert.Null(migration.Down);
	}

	[Fact]
	public void Parse_ShouldReturnMultipleUpQueries_WhenMissingDownQueries()
	{
		// Arrange
		const string content = """
			-- # Migration Up
			SELECT 1;
			SELECT 2;
			SELECT 3;
			""";

		// Act
		var migration = _sut.Parse(content);

		// Assert
		Assert.Equivalent(new[] { "SELECT 1;\nSELECT 2;\nSELECT 3;" }, migration.Up);
		Assert.Null(migration.Down);
	}

	[Fact]
	public void Parse_ShouldReturnMultipleQueries_WhenGivenDownQueriesToo()
	{
		// Arrange
		const string content = """
			-- # Migration Up
			SELECT 1;
			SELECT 2;
			SELECT 3;
			-- # Migration Down
			SELECT 4;
			SELECT 5;
			""";

		// Act
		var migration = _sut.Parse(content);

		// Assert
		Assert.Equivalent(new[] { "SELECT 1;\nSELECT 2;\nSELECT 3;" }, migration.Up);
		Assert.Equivalent(new[] { "SELECT 4;\nSELECT 5;" }, migration.Down);
	}

	[Fact]
	public void Parse_Throws_WhenQueriesBeforeUpComment()
	{
		// Arrange
		const string content = """
			SELECT 1;
			-- # Migration Up
			SELECT 2;
			SELECT 3;
			""";

		// Act
		var fn = () => _sut.Parse(content);

		// Assert
		var e = Assert.Throws<InvalidOperationException>(fn);
		Assert.Equal("Up content found before '-- # Migration Up' comment", e.Message);
	}

	[Fact]
	public void Parse_Throws_WhenMultipleUpComments()
	{
		// Arrange
		const string content = """
			-- # Migration Up
			SELECT 1;
			-- # Migration Up
			SELECT 2;
			SELECT 3;
			""";

		// Act
		var fn = () => _sut.Parse(content);

		// Assert
		var e = Assert.Throws<InvalidOperationException>(fn);
		Assert.Equal("Duplicate '-- # Migration Up' comment found", e.Message);
	}

	[Fact]
	public void Parse_Throws_WhenMultipleDownComments()
	{
		// Arrange
		const string content = """
			-- # Migration Up
			SELECT 1;
			-- # Migration Down
			SELECT 2;
			-- # Migration Down
			SELECT 3;
			""";

		// Act
		var fn = () => _sut.Parse(content);

		// Assert
		var e = Assert.Throws<InvalidOperationException>(fn);
		Assert.Equal("Duplicate '-- # Migration Down' comment found", e.Message);
	}

	[Fact]
	public void Parse_Throws_WhenDownCommentBeforeUp()
	{
		// Arrange
		const string content = """
			-- # Migration Down
			SELECT 1;
			-- # Migration Up
			SELECT 2;
			""";

		// Act
		var fn = () => _sut.Parse(content);

		// Assert
		var e = Assert.Throws<InvalidOperationException>(fn);
		Assert.Equal("'-- # Migration Down' comment found before '-- # Migration Up' comment", e.Message);
	}
}
