using DKX.Migrations.Utils;

namespace DKX.Migrations.Tests.Unit.Utils;

public sealed class MigrationGeneratorTest
{
	[Theory]
	[MemberData(nameof(GetData))]
	public void Generate_AutoIncrement(string mask, int migrationsCount, DateTime now, string? title, string expected)
	{
		Assert.Equal(expected, MigrationGenerator.Generate(mask, migrationsCount, now, title));
	}

	public static IEnumerable<object?[]> GetData()
	{
		var now = DateTime.Parse("2020-01-02 03:04:05");

		return new[]
		{
			new object?[] { "{autoIncrement}", 0, now, null, "1" },
			new object?[] { "{autoIncrement}", 5, now, null, "6" },
			new object?[] { "{autoIncrement:5}", 0, now, null, "00001" },
			new object?[] { "{autoIncrement:5}", 15, now, null, "00016" },
			new object?[] { "{title}", 0, now, null, "" },
			new object?[] { "{title}", 0, now, "Init", "Init" },
			new object?[] { "{year}", 0, now, null, "2020" },
			new object?[] { "{month}", 0, now, null, "01" },
			new object?[] { "{day}", 0, now, null, "02" },
			new object?[] { "{hour}", 0, now, null, "03" },
			new object?[] { "{minute}", 0, now, null, "04" },
			new object?[] { "{second}", 0, now, null, "05" },
			new object?[]
			{
				"{year}/{month}/{year}-{month}-{day}_{hour}-{minute}-{second}_{title}",
				0,
				now,
				"Init",
				"2020/01/2020-01-02_03-04-05_Init",
			},
			new object?[] { "{autoIncrement:5}_{title}", 12, now, "Init", "00013_Init" },
		};
	}
}
