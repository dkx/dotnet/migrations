using DKX.Migrations.Platforms;

namespace DKX.Migrations.Tests.Unit.Platforms;

public sealed class PlatformFactoryTest
{
	[Fact]
	public void Create_Sqlite()
	{
		// Act
		var platform = PlatformFactory.Create(Driver.Sqlite, "");

		// Assert
		Assert.IsType<SqlitePlatform>(platform);
	}

	[Fact]
	public void Create_Mysql()
	{
		// Act
		var platform = PlatformFactory.Create(Driver.MySql, "");

		// Assert
		Assert.IsType<MysqlPlatform>(platform);
	}

	[Fact]
	public void Create_Postgres()
	{
		// Act
		var platform = PlatformFactory.Create(Driver.Pgsql, "Host=myserver;Username=mylogin;Password=mypass;Database=mydatabase");

		// Assert
		Assert.IsType<PostgresPlatform>(platform);
	}

	[Fact]
	public void Create_MsSql()
	{
		// Act
		var platform = PlatformFactory.Create(Driver.MsSql, "");

		// Assert
		Assert.IsType<MsSqlPlatform>(platform);
	}
}
