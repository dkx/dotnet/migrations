using DKX.Migrations.Loaders;
using DKX.Migrations.Migrations;

namespace DKX.Migrations.Tests.Unit.Loaders;

public sealed class StaticMigrationsLoaderTest
{
	[Fact]
	public void LoadMigrations()
	{
		// Arrange
		var migrations = new IMigration[] { };
		var loader = new StaticMigrationsLoader(migrations);

		// Act
		var found = loader.LoadMigrations();

		// Assert
		Assert.Equal(migrations, found);
	}
}
