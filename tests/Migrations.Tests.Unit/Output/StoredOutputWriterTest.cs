using DKX.Migrations.Output;

namespace DKX.Migrations.Tests.Unit.Output;

public sealed class StoredOutputWriterTest
{
	[Fact]
	public void WriteLine()
	{
		// Arrange
		var output = new StoredOutputWriter();
		output.WriteLine();
		output.WriteLine("Hello world");

		// Act
		var result = output.Storage;

		// Assert
		Assert.Equal(new[] { "", "Hello world" }, result);
	}
}
