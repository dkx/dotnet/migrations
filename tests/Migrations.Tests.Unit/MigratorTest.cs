﻿using System.Data;
using DKX.Migrations.Configurations;
using DKX.Migrations.Loaders;
using DKX.Migrations.Migrations;
using DKX.Migrations.Output;
using DKX.Migrations.Parsing;
using DKX.Migrations.Platforms;
using DKX.Migrations.Watch;
using NSubstitute;

namespace DKX.Migrations.Tests.Unit;

public sealed class MigratorTest
{
	private readonly Migrator _sut;

	private readonly IPlatform _platform = Substitute.For<IPlatform>();

	private readonly IMigrationsLoader _loader = Substitute.For<IMigrationsLoader>();

	public MigratorTest()
	{
		_sut = new Migrator(
			_platform,
			new Configuration { Driver = Driver.Sqlite, ConnectionString = "Data Source=:memory:" },
			_loader,
			new StoredOutputWriter(),
			new Parser(),
			new StaticWatchFactory()
		);
	}

	[Fact]
	public void GetNextMigrations_ShouldReturnEmptyPlan_WhenNoMigrationsAndNoExecuted()
	{
		// Arrange
		_platform
			.GetLastExecutedVersion(Arg.Any<IDbTransaction>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>())
			.Returns((string?)null);
		_loader.LoadMigrations(Arg.Any<IReadOnlyCollection<string>?>()).Returns(Array.Empty<IMigration>());

		// Act
		var plan = _sut.GetNextMigrations(null);

		// Assert
		Assert.Equal(Direction.Up, plan.Direction);
		Assert.Empty(plan.Migrations);
	}

	[Fact]
	public void GetNextMigrations_ShouldReturnAllMigrations_WhenNoExecuted()
	{
		// Arrange
		var migrations = new[]
		{
			new StaticMigration
			{
				Group = "structure",
				Version = "1",
				Content = "SELECT 1",
			},
			new StaticMigration
			{
				Group = "structure",
				Version = "2",
				Content = "SELECT 2",
			},
		};
		_platform
			.GetLastExecutedVersion(Arg.Any<IDbTransaction>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>())
			.Returns((string?)null);
		_loader.LoadMigrations(Arg.Any<IReadOnlyCollection<string>?>()).Returns(migrations);

		// Act
		var plan = _sut.GetNextMigrations(null);

		// Assert
		Assert.Equal(Direction.Up, plan.Direction);
		Assert.Equivalent(migrations, plan.Migrations);
	}

	[Fact]
	public void GetNextMigrations_ShouldReturnEmptyPlan_WhenMigratingUpToCurrent()
	{
		// Arrange
		var migrations = new[]
		{
			new StaticMigration
			{
				Group = "structure",
				Version = "1",
				Content = "SELECT 1",
			},
		};
		_platform.GetLastExecutedVersion(Arg.Any<IDbTransaction>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>()).Returns("1");
		_loader.LoadMigrations(Arg.Any<IReadOnlyCollection<string>?>()).Returns(migrations);

		// Act
		var plan = _sut.GetNextMigrations(null, new MigrateOptions { ToVersion = "1" });

		// Assert
		Assert.Equal(Direction.Up, plan.Direction);
		Assert.Empty(plan.Migrations);
	}

	[Fact]
	public void GetNextMigrations_ShouldReturnNextMigrations_WhenGivenToVersion()
	{
		// Arrange
		var migrations = new[]
		{
			new StaticMigration
			{
				Group = "structure",
				Version = "1",
				Content = "SELECT 1",
			},
			new StaticMigration
			{
				Group = "structure",
				Version = "2",
				Content = "SELECT 2",
			},
			new StaticMigration
			{
				Group = "structure",
				Version = "3",
				Content = "SELECT 3",
			},
			new StaticMigration
			{
				Group = "structure",
				Version = "4",
				Content = "SELECT 4",
			},
		};
		_platform.GetLastExecutedVersion(Arg.Any<IDbTransaction>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>()).Returns("1");
		_loader.LoadMigrations(Arg.Any<IReadOnlyCollection<string>?>()).Returns(migrations);

		// Act
		var plan = _sut.GetNextMigrations(null, new MigrateOptions { ToVersion = "3" });

		// Assert
		Assert.Equal(Direction.Up, plan.Direction);
		Assert.Equivalent(
			new[]
			{
				new StaticMigration
				{
					Group = "structure",
					Version = "2",
					Content = "SELECT 2",
				},
				new StaticMigration
				{
					Group = "structure",
					Version = "3",
					Content = "SELECT 3",
				},
			},
			plan.Migrations
		);
	}

	[Fact]
	public void GetNextMigrations_ShouldReturnAllNextMigrations_WhenNotSpecifyingToVersion()
	{
		// Arrange
		var migrations = new[]
		{
			new StaticMigration
			{
				Group = "structure",
				Version = "1",
				Content = "SELECT 1",
			},
			new StaticMigration
			{
				Group = "structure",
				Version = "2",
				Content = "SELECT 2",
			},
			new StaticMigration
			{
				Group = "structure",
				Version = "3",
				Content = "SELECT 3",
			},
			new StaticMigration
			{
				Group = "structure",
				Version = "4",
				Content = "SELECT 4",
			},
		};
		_platform.GetLastExecutedVersion(Arg.Any<IDbTransaction>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>()).Returns("1");
		_loader.LoadMigrations(Arg.Any<IReadOnlyCollection<string>?>()).Returns(migrations);

		// Act
		var plan = _sut.GetNextMigrations(null);

		// Assert
		Assert.Equal(Direction.Up, plan.Direction);
		Assert.Equivalent(
			new[]
			{
				new StaticMigration
				{
					Group = "structure",
					Version = "2",
					Content = "SELECT 2",
				},
				new StaticMigration
				{
					Group = "structure",
					Version = "3",
					Content = "SELECT 3",
				},
				new StaticMigration
				{
					Group = "structure",
					Version = "4",
					Content = "SELECT 4",
				},
			},
			plan.Migrations
		);
	}

	[Fact]
	public void GetNextMigrations_ShouldReturnAllNextMigrations_WhenGivenSpecificGroup()
	{
		// Arrange
		var migrations = new[]
		{
			new StaticMigration
			{
				Group = "structure",
				Version = "1",
				Content = "SELECT 1",
			},
			new StaticMigration
			{
				Group = "basic-data",
				Version = "2",
				Content = "SELECT 2",
			},
			new StaticMigration
			{
				Group = "structure",
				Version = "3",
				Content = "SELECT 3",
			},
			new StaticMigration
			{
				Group = "basic-data",
				Version = "4",
				Content = "SELECT 4",
			},
		};
		_platform.GetLastExecutedVersion(Arg.Any<IDbTransaction>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>()).Returns("1");
		_loader.LoadMigrations(Arg.Any<IReadOnlyCollection<string>?>()).Returns(migrations);

		// Act
		var plan = _sut.GetNextMigrations(null, new MigrateOptions { Groups = new[] { "basic-data" } });

		// Assert
		Assert.Equal(Direction.Up, plan.Direction);
		Assert.Equivalent(
			new[]
			{
				new StaticMigration
				{
					Group = "basic-data",
					Version = "2",
					Content = "SELECT 2",
				},
				new StaticMigration
				{
					Group = "basic-data",
					Version = "4",
					Content = "SELECT 4",
				},
			},
			plan.Migrations
		);
	}

	[Fact]
	public void GetNextMigrations_ShouldReturnPreviousMigrations_WhenGivenPreviousToVersion()
	{
		// Arrange
		var migrations = new[]
		{
			new StaticMigration
			{
				Group = "structure",
				Version = "1",
				Content = "SELECT 1",
			},
			new StaticMigration
			{
				Group = "basic-data",
				Version = "2",
				Content = "SELECT 2",
			},
			new StaticMigration
			{
				Group = "structure",
				Version = "3",
				Content = "SELECT 3",
			},
			new StaticMigration
			{
				Group = "basic-data",
				Version = "4",
				Content = "SELECT 4",
			},
		};
		_platform.GetLastExecutedVersion(Arg.Any<IDbTransaction>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>()).Returns("3");
		_loader.LoadMigrations(Arg.Any<IReadOnlyCollection<string>?>()).Returns(migrations);

		// Act
		var plan = _sut.GetNextMigrations(null, new MigrateOptions { ToVersion = "1" });

		// Assert
		Assert.Equal(Direction.Down, plan.Direction);
		Assert.Equivalent(
			new[]
			{
				new StaticMigration
				{
					Group = "structure",
					Version = "3",
					Content = "SELECT 3",
				},
				new StaticMigration
				{
					Group = "basic-data",
					Version = "2",
					Content = "SELECT 2",
				},
			},
			plan.Migrations
		);
	}

	[Fact]
	public void GetNextMigrations_ShouldReturnPreviousMigrationsUpToInit_WhenGivenZero()
	{
		// Arrange
		var migrations = new[]
		{
			new StaticMigration
			{
				Group = "structure",
				Version = "1",
				Content = "SELECT 1",
			},
			new StaticMigration
			{
				Group = "basic-data",
				Version = "2",
				Content = "SELECT 2",
			},
			new StaticMigration
			{
				Group = "structure",
				Version = "3",
				Content = "SELECT 3",
			},
			new StaticMigration
			{
				Group = "basic-data",
				Version = "4",
				Content = "SELECT 4",
			},
		};
		_platform.GetLastExecutedVersion(Arg.Any<IDbTransaction>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>()).Returns("3");
		_loader.LoadMigrations(Arg.Any<IReadOnlyCollection<string>?>()).Returns(migrations);

		// Act
		var plan = _sut.GetNextMigrations(null, new MigrateOptions { ToVersion = "0" });

		// Assert
		Assert.Equal(Direction.Down, plan.Direction);
		Assert.Equivalent(
			new[]
			{
				new StaticMigration
				{
					Group = "structure",
					Version = "3",
					Content = "SELECT 3",
				},
				new StaticMigration
				{
					Group = "basic-data",
					Version = "2",
					Content = "SELECT 2",
				},
				new StaticMigration
				{
					Group = "structure",
					Version = "1",
					Content = "SELECT 1",
				},
			},
			plan.Migrations
		);
	}

	[Fact]
	public void GetNextMigrations_Throws_WhenSpecificMigrationNotExists()
	{
		// Arrange
		_platform
			.GetLastExecutedVersion(Arg.Any<IDbTransaction>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>())
			.Returns((string?)null);

		// Act
		var fn = () => _sut.GetNextMigrations(null, new MigrateOptions { ToVersion = "2" });

		// Assert
		var e = Assert.Throws<Exception>(fn);
		Assert.Equal($"Migration '2' does not exists", e.Message);
	}
}
