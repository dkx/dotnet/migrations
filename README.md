# DKX.Migrations

Simple database migrations for ADO.NET written in .sql files.

* Migrations in .sql files
* Independent migration groups (can be enabled/disabled)
* Forward only - no rollback or down migrations

This package is inspired by [doctrine](https://www.doctrine-project.org/projects/doctrine-migrations/en/2.2/reference/introduction.html)
and [nextras](https://nextras.org/migrations/docs/3.1/) migrations.

## Installation

```bash
$ dotnet tool install DKX.Migrations.Cli --version 0.0.1-alpha.20
```

*Check [tags](https://gitlab.com/dkx/dotnet/migrations/-/tags) for the latest version.*

## Configuration

Create default configuration file and update it (`./migrations.json`):

```bash
$ dotnet migrations init
```

## Writing migrations

Migrations are sorted by file name before executing them, so it's a good idea to name them eg. with current date:

* `Data/Migrations/2020-03-25_21-30-15_Init.sql`
* `Data/Migrations/2020-04-01_15-00-30_AddUsers.sql`

Or simply use incremental numbers:

* `Data/Migrations/00001_Init.sql`
* `Data/Migrations/00002_AddUsers.sql`

Optionally, you can have migrations in any subdirectory, for example:

* `Data/Migrations`
    * `2020`
        * `03`
            * `2020-03-25_21-30-15_Init.sql`
        * `04`
            * `2020-04-01_15-00-30_AddUsers.sql`

**Migration file example:**

```sql
-- # Migration Up
CREATE TABLE users (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    name TEXT NOT NULL
);

-- # Migration Down
DROP TABLE users;
```

This migration file will be automatically split into two parts:

* `Up` - executed when migrating forward
* `Down` - executed when migrating backward

## Run migrations

```bash
$ dotnet migrations migrate
```

## Features

* [Supported connections](./docs/connections.md)
* [Configuration](./docs/configuration.md)
* [Groups](./docs/groups.md)
* [Status](./docs/status.md)
* [Migration generating](./docs/generate.md)
* [Dry run](./docs/dry-run.md)
* [Docker](./docs/docker.md)
