﻿using System;

namespace DKX.Migrations.Watch;

public interface IWatchFactory
{
	IWatch Create();
}
