﻿using System;

namespace DKX.Migrations.Watch;

public interface IWatch : IDisposable
{
	TimeSpan Elapsed { get; }
}
