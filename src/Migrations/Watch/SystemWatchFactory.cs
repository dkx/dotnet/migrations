﻿using System;

namespace DKX.Migrations.Watch;

public sealed class SystemWatchFactory : IWatchFactory
{
	public IWatch Create()
	{
		return new SystemWatch();
	}
}
