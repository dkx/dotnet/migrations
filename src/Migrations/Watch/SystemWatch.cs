﻿using System;
using System.Diagnostics;

namespace DKX.Migrations.Watch;

public sealed class SystemWatch : IWatch
{
	private readonly Stopwatch _stopwatch;

	public SystemWatch()
	{
		_stopwatch = Stopwatch.StartNew();
	}

	public void Dispose()
	{
		_stopwatch.Stop();
	}

	public TimeSpan Elapsed => _stopwatch.Elapsed;
}
