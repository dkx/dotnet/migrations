﻿using System;

namespace DKX.Migrations.Watch;

public sealed class StaticWatch : IWatch
{
	private static readonly TimeSpan StaticElapsed = TimeSpan.FromMilliseconds(42);

	public void Dispose() { }

	public TimeSpan Elapsed => StaticElapsed;
}
