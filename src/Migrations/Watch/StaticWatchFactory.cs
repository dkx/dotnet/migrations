﻿namespace DKX.Migrations.Watch;

public sealed class StaticWatchFactory : IWatchFactory
{
	public IWatch Create()
	{
		return new StaticWatch();
	}
}
