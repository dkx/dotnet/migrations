using System.Collections.Generic;
using DKX.Migrations.Migrations;

namespace DKX.Migrations.Loaders;

public interface IMigrationsLoader
{
	IReadOnlyCollection<IMigration> LoadMigrations(IReadOnlyCollection<string>? groups = null);
}
