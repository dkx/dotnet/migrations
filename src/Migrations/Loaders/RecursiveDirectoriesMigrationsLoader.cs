using System.Collections.Generic;
using System.IO;
using System.Linq;
using DKX.Migrations.Configurations;
using DKX.Migrations.Migrations;

namespace DKX.Migrations.Loaders;

public sealed class RecursiveDirectoriesMigrationsLoader : IMigrationsLoader
{
	private readonly string _mask;

	private readonly IReadOnlyCollection<IGroupConfiguration> _groups;

	public RecursiveDirectoriesMigrationsLoader(string mask, IReadOnlyCollection<IGroupConfiguration> groups)
	{
		_mask = mask;
		_groups = groups;
	}

	public IReadOnlyCollection<IMigration> LoadMigrations(IReadOnlyCollection<string>? groups = null)
	{
		var result = new List<IMigration>();

		foreach (var group in SelectGroups(groups).Where(g => g.Enabled))
		{
			var migrations = Directory
				.GetFiles(group.Directory, _mask, SearchOption.AllDirectories)
				.Select(file => new FileMigration
				{
					Group = group.Name,
					Version = Path.GetFileNameWithoutExtension(file),
					Path = file,
				});

			result.AddRange(migrations);
		}

		return result;
	}

	private IReadOnlyCollection<IGroupConfiguration> SelectGroups(IEnumerable<string>? migrateGroups)
	{
		return migrateGroups is null
			? _groups
			: _groups
				.Select(d => new GroupConfiguration
				{
					Name = d.Name,
					Directory = d.Directory,
					Enabled = migrateGroups.Contains(d.Name),
				})
				.ToList();
	}
}
