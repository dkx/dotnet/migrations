using System.Collections.Generic;
using System.Linq;
using DKX.Migrations.Migrations;

namespace DKX.Migrations.Loaders;

public sealed class StaticMigrationsLoader : IMigrationsLoader
{
	private readonly IReadOnlyCollection<IMigration> _migrations;

	public StaticMigrationsLoader(IReadOnlyCollection<IMigration> migrations)
	{
		_migrations = migrations;
	}

	public IReadOnlyCollection<IMigration> LoadMigrations(IReadOnlyCollection<string>? groups = null)
	{
		if (groups is not null)
		{
			return _migrations.Where(m => groups.Contains(m.Group)).ToList();
		}

		return _migrations;
	}
}
