namespace DKX.Migrations.Output;

public interface IOutputWriter
{
	void WriteLine();

	void WriteLine(string value);

	string? ReadLine();
}
