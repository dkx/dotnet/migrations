using System.Collections.Generic;

namespace DKX.Migrations.Output;

public sealed class StoredOutputWriter : IOutputWriter
{
	private string? _input;

	public IList<string> Storage { get; } = new List<string>();

	public void SetInput(string? input)
	{
		_input = input;
	}

	public void Clear()
	{
		Storage.Clear();
	}

	public void WriteLine()
	{
		Storage.Add("");
	}

	public void WriteLine(string value)
	{
		Storage.Add(value);
	}

	public string? ReadLine()
	{
		return _input;
	}
}
