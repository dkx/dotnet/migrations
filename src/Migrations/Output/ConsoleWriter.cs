using System;

namespace DKX.Migrations.Output;

public sealed class ConsoleWriter : IOutputWriter
{
	public void WriteLine()
	{
		Console.WriteLine();
	}

	public void WriteLine(string value)
	{
		Console.WriteLine(value);
	}

	public string? ReadLine()
	{
		return Console.ReadLine();
	}
}
