using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DKX.Migrations.Configurations;
using DKX.Migrations.Events;
using DKX.Migrations.Loaders;
using DKX.Migrations.Migrations;
using DKX.Migrations.Output;
using DKX.Migrations.Parsing;
using DKX.Migrations.Platforms;
using DKX.Migrations.Watch;

namespace DKX.Migrations;

public sealed class Migrator : IDisposable
{
	public event EventHandler<CurrentStateLoadedEventArgs>? CurrentStateLoaded;

	public event EventHandler<BeforeMigrationEventArgs>? BeforeMigration;

	public event EventHandler<AfterMigrationEventArgs>? AfterMigration;

	public event EventHandler<BeforeQueryEventArgs>? BeforeQuery;

	public event EventHandler<NoQueriesEventArgs>? NoQueries;

	private readonly IPlatform _platform;

	private readonly Configuration _configuration;

	private readonly IMigrationsLoader _migrationsLoader;

	private readonly IOutputWriter _output;

	private readonly IParser _parser;

	private readonly IWatchFactory _watchFactory;

	private bool _initialized;

	public Migrator(
		IPlatform platform,
		Configuration configuration,
		IMigrationsLoader migrations,
		IOutputWriter output,
		IParser parser,
		IWatchFactory watchFactory
	)
	{
		_platform = platform;
		_configuration = configuration;
		_migrationsLoader = migrations;
		_output = output;
		_parser = parser;
		_watchFactory = watchFactory;
	}

	public void Dispose()
	{
		_platform.Dispose();
	}

	public MigrationResult Migrate(MigrateOptions? options = null)
	{
		var opts = options ?? new MigrateOptions();

		InitializeStorage(null);

		var last = FindLastExecutedMigration(null, opts.Groups);
		var plan = GetNextMigrations(null, opts);

		Debug(
			opts,
			last is null ? "No previously executed migration found" : "Found last executed migration " + last.Group + "/" + last.Version
		);

		foreach (var n in plan.Migrations)
		{
			Debug(opts, "Found next migration " + n.Group + "/" + n.Version);
		}

		var currentStateEvent = new CurrentStateLoadedEventArgs(opts, last, plan);
		CurrentStateLoaded?.Invoke(this, currentStateEvent);
		if (currentStateEvent.Abort)
		{
			Debug(opts, "Aborted by event handler");
			return new MigrationResult { Aborted = true };
		}

		IDbTransaction? globalTransaction = null;
		if (_configuration.AllOrNothing && opts.DryRun != true)
		{
			Debug(opts, "Begin global transaction");
			globalTransaction = _platform.BeginTransaction();
		}

		try
		{
			foreach (var migration in plan.Migrations)
			{
				Debug(opts, "Starting migration " + migration.Group + "/" + migration.Version);
				RunMigration(opts, migration, plan.Direction, globalTransaction);
			}

			if (globalTransaction is not null)
			{
				Debug(opts, "Commit global transaction");
				globalTransaction.Commit();
			}
		}
		catch (Exception e)
		{
			Debug(opts, "Error: " + e.Message);

			if (globalTransaction is not null)
			{
				Debug(opts, "Rollback global transaction");

				try
				{
					globalTransaction.Rollback();
				}
				catch (Exception innerException)
				{
					Debug(opts, "Could not rollback global transaction: " + innerException.Message);
				}
			}

			throw;
		}

		if (globalTransaction is not null)
		{
			Debug(opts, "Dispose global transaction");
			globalTransaction.Dispose();
		}

		return new MigrationResult { MigrationsCount = plan.Migrations.Count };
	}

	public IMigration? FindLastExecutedMigration(IDbTransaction? transaction, IReadOnlyCollection<string>? groups = null)
	{
		InitializeStorage(transaction);

		var lastVersion = _platform.GetLastExecutedVersion(
			transaction,
			_configuration.StorageTableName,
			_configuration.StorageIdColumnName,
			_configuration.StorageVersionColumnName
		);

		if (lastVersion is null)
		{
			return null;
		}

		var migrations = GetMigrations(groups);
		var lastMigration = migrations.FirstOrDefault(m => m.Version == lastVersion);
		if (lastMigration is null)
		{
			throw new Exception($"Migration {lastVersion} does not exists");
		}

		return lastMigration;
	}

	public MigrationsPlan GetNextMigrations(IDbTransaction? transaction, MigrateOptions? options = null)
	{
		var opts = options ?? new MigrateOptions();

		InitializeStorage(transaction);

		var migrations = GetMigrations(opts.Groups).ToArray();
		var current = FindLastExecutedMigration(transaction, opts.Groups);

		if (GetToMigration(opts, migrations) is not { } toVersion || migrations.Length < 1)
		{
			return new MigrationsPlan();
		}

		var currentIndex = current is null ? -1 : Array.IndexOf(migrations, current);
		var toIndex = toVersion is ZeroMigration ? -1 : Array.IndexOf(migrations, toVersion);

		if (currentIndex == toIndex)
		{
			return new MigrationsPlan();
		}

		if (currentIndex < toIndex)
		{
			return new MigrationsPlan
			{
				Direction = Direction.Up,
				Migrations = migrations.Skip(currentIndex + 1).Take(toIndex - currentIndex).ToList(),
			};
		}

		return new MigrationsPlan
		{
			Direction = Direction.Down,
			Migrations = migrations.Skip(toIndex + 1).Take(currentIndex - toIndex).Reverse().ToList(),
		};

		static IMigration? GetToMigration(MigrateOptions options, IReadOnlyCollection<IMigration> migrations)
		{
			if (options.ToVersion is { } name)
			{
				if (name == "0")
				{
					return ZeroMigration.Instance;
				}

				if (migrations.SingleOrDefault(x => x.Version == name) is not { } found)
				{
					throw new Exception($"Migration '{name}' does not exists");
				}

				return found;
			}

			return migrations.LastOrDefault();
		}
	}

	private void InitializeStorage(IDbTransaction? transaction)
	{
		if (_initialized)
		{
			return;
		}

		_platform.TryCreateTableStorage(
			transaction,
			_configuration.StorageTableName,
			_configuration.StorageIdColumnName,
			_configuration.StorageVersionColumnName,
			_configuration.StorageExecutedAtColumnName
		);

		_initialized = true;
	}

	private IReadOnlyCollection<IMigration> GetMigrations(IReadOnlyCollection<string>? groups)
	{
		return _migrationsLoader.LoadMigrations(groups).OrderBy(m => m.Version).ToList();
	}

	private void RunMigration(MigrateOptions options, IMigration migration, Direction direction, IDbTransaction? globalTransaction)
	{
		BeforeMigration?.Invoke(this, new BeforeMigrationEventArgs(migration));

		var content = _parser.Parse(migration.GetContent());
		IReadOnlyCollection<string> queries;

		if (direction == Direction.Down)
		{
			if (content.Down is not { } downMigrations)
			{
				throw new Exception("Down migration is not defined");
			}

			queries = downMigrations;
		}
		else
		{
			queries = content.Up;
		}

		if (queries.Count < 1)
		{
			Debug(options, "No queries to execute");
			NoQueries?.Invoke(this, new NoQueriesEventArgs());
		}

		using var watch = _watchFactory.Create();

		IDbTransaction? localTransaction = null;
		if (globalTransaction is null && options.DryRun != true)
		{
			Debug(options, "Begin local transaction");
			localTransaction = _platform.BeginTransaction();
		}

		var activeTransaction = globalTransaction ?? localTransaction;
		var affectedRows = 0;

		try
		{
			var count = 0;
			foreach (var query in queries)
			{
				if (options.DryRun != true)
				{
					Debug(options, "Executing query " + migration.Group + "/" + migration.Version + "[" + count + "]");
				}

				BeforeQuery?.Invoke(this, new BeforeQueryEventArgs(query));

				if (options.DryRun != true)
				{
					var result = _platform.Execute(activeTransaction, query);
					affectedRows += result;
					Debug(options, "Query " + migration.Group + "/" + migration.Version + "[" + count + "] affected rows: " + result);
				}

				count++;
			}

			if (options.DryRun != true)
			{
				if (direction == Direction.Up)
				{
					Debug(options, "Inserting version record for " + migration.Group + "/" + migration.Version);
					_platform.InsertVersionRecord(
						activeTransaction,
						_configuration.StorageTableName,
						_configuration.StorageVersionColumnName,
						_configuration.StorageExecutedAtColumnName,
						migration.Version,
						DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Utc)
					);
				}
				else
				{
					Debug(options, "Removing version record for " + migration.Group + "/" + migration.Version);
					_platform.RemoveVersionRecord(
						activeTransaction,
						_configuration.StorageTableName,
						_configuration.StorageIdColumnName,
						_configuration.StorageVersionColumnName,
						migration.Version
					);
				}
			}

			if (localTransaction is not null)
			{
				Debug(options, "Commit local transaction");
				localTransaction.Commit();
			}
		}
		catch (Exception e)
		{
			Debug(options, "Error: " + e.Message);

			if (localTransaction is not null)
			{
				Debug(options, "Rollback local transaction");

				try
				{
					localTransaction.Rollback();
				}
				catch (Exception innerException)
				{
					Debug(options, "Could not rollback local transaction: " + innerException.Message);
				}
			}

			throw;
		}

		if (localTransaction is not null)
		{
			Debug(options, "Dispose local transaction");
			localTransaction.Dispose();
		}

		AfterMigration?.Invoke(this, new AfterMigrationEventArgs(migration, watch.Elapsed, affectedRows));
	}

	private void Debug(MigrateOptions options, string message)
	{
		if (options.Verbose == true)
		{
			_output.WriteLine("DEBUG: " + message);
		}
	}
}
