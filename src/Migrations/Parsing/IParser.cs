﻿namespace DKX.Migrations.Parsing;

public interface IParser
{
	Migration Parse(string content);
}
