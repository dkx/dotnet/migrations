﻿using System;
using System.Linq;
using System.Text;

namespace DKX.Migrations.Parsing;

public sealed class Parser : IParser
{
	public Migration Parse(string content)
	{
		var hasUpComment = false;
		var hasDownComment = false;
		var lead = new StringBuilder();
		var up = new StringBuilder();
		var down = new StringBuilder();
		var current = lead;

		var lines = content.Replace("\r", "").Split('\n').Select(x => x.TrimEnd()).Where(x => !string.IsNullOrWhiteSpace(x));
		foreach (var line in lines)
		{
			if (line == "-- # Migration Up")
			{
				if (hasUpComment)
				{
					throw new InvalidOperationException("Duplicate '-- # Migration Up' comment found");
				}

				hasUpComment = true;
				current = up;
				continue;
			}

			if (line == "-- # Migration Down")
			{
				if (hasDownComment)
				{
					throw new InvalidOperationException("Duplicate '-- # Migration Down' comment found");
				}

				if (!hasUpComment)
				{
					throw new InvalidOperationException("'-- # Migration Down' comment found before '-- # Migration Up' comment");
				}

				hasDownComment = true;
				current = down;
				continue;
			}

			current.AppendLine(line);
		}

		if (hasUpComment && lead.Length > 0)
		{
			throw new InvalidOperationException("Up content found before '-- # Migration Up' comment");
		}

		if (lead.Length > 0)
		{
			up = lead;
		}

		var migration = new Migration();

		var upContent = up.ToString().Replace("\r", string.Empty).Trim();
		if (!string.IsNullOrWhiteSpace(upContent))
		{
			migration = migration with { Up = new[] { upContent } };
		}

		if (hasDownComment)
		{
			var downContent = down.ToString().Replace("\r", string.Empty).Trim();
			if (string.IsNullOrWhiteSpace(downContent))
			{
				migration = migration with { Down = Array.Empty<string>() };
			}
			else
			{
				migration = migration with { Down = new[] { downContent } };
			}
		}

		return migration;
	}
}
