﻿using System;
using System.Collections.Generic;

namespace DKX.Migrations.Parsing;

public sealed record Migration
{
	public IReadOnlyCollection<string> Up { get; init; } = Array.Empty<string>();

	public IReadOnlyCollection<string>? Down { get; init; }
}
