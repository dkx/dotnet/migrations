using System.Data;

namespace DKX.Migrations.Platforms;

public sealed class MsSqlPlatform : AdoPlatform
{
	public MsSqlPlatform(IDbConnection connection)
		: base(connection) { }

	public override void TryCreateTableStorage(
		IDbTransaction? transaction,
		string table,
		string idColumnName,
		string versionColumnName,
		string executedAtColumnName
	)
	{
		Execute(
			transaction,
			$"IF OBJECT_ID(N'dbo.{table}', N'U') IS NULL BEGIN CREATE TABLE dbo.{table} ({idColumnName} INT IDENTITY (1, 1) PRIMARY KEY, {versionColumnName} VARCHAR(255) NOT NULL, {executedAtColumnName} DATETIME NOT NULL); END;"
		);
	}

	public override string? GetLastExecutedVersion(IDbTransaction? transaction, string table, string idColumnName, string versionColumnName)
	{
		return ExecuteScalar<string?>(
			transaction,
			$"SELECT {versionColumnName} FROM dbo.{table} ORDER BY {idColumnName} DESC OFFSET 0 ROWS FETCH FIRST 1 ROW ONLY"
		);
	}
}
