namespace DKX.Migrations.Platforms;

public enum Driver
{
	Sqlite,
	MySql,
	Pgsql,
	MsSql,
}
