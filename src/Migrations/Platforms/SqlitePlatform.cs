using System.Data;

namespace DKX.Migrations.Platforms;

public sealed class SqlitePlatform : AdoPlatform
{
	public SqlitePlatform(IDbConnection connection)
		: base(connection) { }

	public override void TryCreateTableStorage(
		IDbTransaction? transaction,
		string table,
		string idColumnName,
		string versionColumnName,
		string executedAtColumnName
	)
	{
		Execute(
			transaction,
			$"CREATE TABLE IF NOT EXISTS {table} ({idColumnName} INTEGER PRIMARY KEY AUTOINCREMENT, {versionColumnName} TEXT NOT NULL UNIQUE, {executedAtColumnName} TEXT NOT NULL)"
		);
	}

	public override string? GetLastExecutedVersion(IDbTransaction? transaction, string table, string idColumnName, string versionColumnName)
	{
		return ExecuteScalar<string?>(transaction, $"SELECT {versionColumnName} FROM {table} ORDER BY {idColumnName} DESC LIMIT 1");
	}
}
