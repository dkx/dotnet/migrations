using System;
using System.Data;

namespace DKX.Migrations.Platforms;

public interface IPlatform : IDisposable
{
	void TryCreateTableStorage(
		IDbTransaction? transaction,
		string table,
		string idColumnName,
		string versionColumnName,
		string executedAtColumnName
	);

	string? GetLastExecutedVersion(IDbTransaction? transaction, string table, string idColumnName, string versionColumnName);

	void InsertVersionRecord(
		IDbTransaction? transaction,
		string table,
		string versionColumnName,
		string executedAtColumnName,
		string version,
		DateTime executedAt
	);

	void RemoveVersionRecord(IDbTransaction? transaction, string table, string idColumnName, string versionColumnName, string version);

	IDbTransaction BeginTransaction();

	int Execute(IDbTransaction? transaction, string sql);
}
