using System;
using System.Data;

namespace DKX.Migrations.Platforms;

public sealed class PostgresPlatform : AdoPlatform
{
	public PostgresPlatform(IDbConnection connection)
		: base(connection) { }

	public override void TryCreateTableStorage(
		IDbTransaction? transaction,
		string table,
		string idColumnName,
		string versionColumnName,
		string executedAtColumnName
	)
	{
		var parts = table.Split('.');
		if (parts.Length == 2)
		{
			Execute(transaction, $"CREATE SCHEMA IF NOT EXISTS {parts[0]}");
		}
		else if (parts.Length != 1)
		{
			throw new ArgumentException($"Invalid table name '{table}'");
		}

		Execute(
			transaction,
			$"CREATE TABLE IF NOT EXISTS {table} ({idColumnName} SERIAL PRIMARY KEY, {versionColumnName} VARCHAR(255) NOT NULL, {executedAtColumnName} TIMESTAMP WITHOUT TIME ZONE NOT NULL)"
		);
	}

	public override string? GetLastExecutedVersion(IDbTransaction? transaction, string table, string idColumnName, string versionColumnName)
	{
		return ExecuteScalar<string?>(transaction, $"SELECT {versionColumnName} FROM {table} ORDER BY {idColumnName} DESC LIMIT 1");
	}
}
