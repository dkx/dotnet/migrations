using System;
using Microsoft.Data.SqlClient;
using Microsoft.Data.Sqlite;
using MySql.Data.MySqlClient;
using Npgsql;

namespace DKX.Migrations.Platforms;

public static class PlatformFactory
{
	public static IPlatform Create(Driver driver, string connection)
	{
		return driver switch
		{
			Driver.Sqlite => new SqlitePlatform(new SqliteConnection(connection)),
			Driver.MySql => new MysqlPlatform(new MySqlConnection(connection)),
			Driver.Pgsql => new PostgresPlatform(new NpgsqlConnection(connection)),
			Driver.MsSql => new MsSqlPlatform(new SqlConnection(connection)),
			_ => throw new ArgumentException($"Can not create IPlatform for unsupported driver {driver.ToString()}"),
		};
	}
}
