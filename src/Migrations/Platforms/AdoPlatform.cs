using System;
using System.Data;

namespace DKX.Migrations.Platforms;

public abstract class AdoPlatform : IPlatform
{
	private readonly IDbConnection _connection;

	protected AdoPlatform(IDbConnection connection)
	{
		_connection = connection;
	}

	public void Dispose()
	{
		_connection.Dispose();
	}

	public abstract void TryCreateTableStorage(
		IDbTransaction? transaction,
		string table,
		string idColumnName,
		string versionColumnName,
		string executedAtColumnName
	);

	public abstract string? GetLastExecutedVersion(
		IDbTransaction? transaction,
		string table,
		string idColumnName,
		string versionColumnName
	);

	public void InsertVersionRecord(
		IDbTransaction? transaction,
		string table,
		string versionColumnName,
		string executedAtColumnName,
		string version,
		DateTime executedAt
	)
	{
		using var command = _connection.CreateCommand();

		if (transaction is not null)
		{
			command.Transaction = transaction;
		}

		var paramVersion = command.CreateParameter();
		paramVersion.ParameterName = "Version";
		paramVersion.Value = version;
		paramVersion.Size = version.Length;
		paramVersion.DbType = DbType.String;

		var paramExecutedAt = command.CreateParameter();
		paramExecutedAt.ParameterName = "ExecutedAt";
		paramExecutedAt.Value = executedAt;
		paramExecutedAt.DbType = DbType.DateTime;

		command.CommandText = $"INSERT INTO {table} ({versionColumnName}, {executedAtColumnName}) VALUES (@Version, @ExecutedAt)";
		command.Parameters.Add(paramVersion);
		command.Parameters.Add(paramExecutedAt);

		command.Prepare();
		command.ExecuteNonQuery();
	}

	public void RemoveVersionRecord(
		IDbTransaction? transaction,
		string table,
		string idColumnName,
		string versionColumnName,
		string version
	)
	{
		var latestVersion = GetLastExecutedVersion(transaction, table, idColumnName, versionColumnName);
		if (latestVersion != version)
		{
			throw new InvalidOperationException($"Cannot remove version {version} because the latest version is {latestVersion}");
		}

		using var command = _connection.CreateCommand();

		if (transaction is not null)
		{
			command.Transaction = transaction;
		}

		var paramVersion = command.CreateParameter();
		paramVersion.ParameterName = "Version";
		paramVersion.Value = version;
		paramVersion.Size = version.Length;
		paramVersion.DbType = DbType.String;

		command.CommandText = $"DELETE FROM {table} WHERE {versionColumnName} = @Version";
		command.Parameters.Add(paramVersion);

		command.Prepare();
		command.ExecuteNonQuery();
	}

	public IDbTransaction BeginTransaction()
	{
		return _connection.BeginTransaction();
	}

	public int Execute(IDbTransaction? transaction, string sql)
	{
		if (_connection.State != ConnectionState.Open)
		{
			_connection.Open();
		}

		using var command = _connection.CreateCommand();
		command.CommandText = sql;

		if (transaction is not null)
		{
			command.Transaction = transaction;
		}

		return command.ExecuteNonQuery();
	}

	protected TResult ExecuteScalar<TResult>(IDbTransaction? transaction, string sql)
	{
		if (_connection.State != ConnectionState.Open)
		{
			_connection.Open();
		}

		using var command = _connection.CreateCommand();
		command.CommandText = sql;

		if (transaction is not null)
		{
			command.Transaction = transaction;
		}

		var result = command.ExecuteScalar();
		if (result is DBNull or null)
		{
			return default!;
		}

		if (result is TResult typedResult)
		{
			return typedResult;
		}

		throw new InvalidOperationException($"Expected {typeof(TResult).Name} but got {result?.GetType().Name}");
	}
}
