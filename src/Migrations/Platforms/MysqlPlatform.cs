using System.Data;

namespace DKX.Migrations.Platforms;

public sealed class MysqlPlatform : AdoPlatform
{
	public MysqlPlatform(IDbConnection connection)
		: base(connection) { }

	public override void TryCreateTableStorage(
		IDbTransaction? transaction,
		string table,
		string idColumnName,
		string versionColumnName,
		string executedAtColumnName
	)
	{
		Execute(
			transaction,
			$"CREATE TABLE IF NOT EXISTS {table} ({idColumnName} INTEGER NOT NULL AUTO_INCREMENT, {versionColumnName} VARCHAR(255) NOT NULL, {executedAtColumnName} DATETIME NOT NULL, PRIMARY KEY ({idColumnName})) ENGINE=InnoDB"
		);
	}

	public override string? GetLastExecutedVersion(IDbTransaction? transaction, string table, string idColumnName, string versionColumnName)
	{
		return ExecuteScalar<string?>(transaction, $"SELECT {versionColumnName} FROM {table} ORDER BY {idColumnName} DESC LIMIT 1");
	}
}
