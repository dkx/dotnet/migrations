using System;
using System.Text.RegularExpressions;

namespace DKX.Migrations.Utils;

public static class MigrationGenerator
{
	public static string Generate(string mask, int migrationsCount, DateTime now, string? title = null)
	{
		var next = (migrationsCount + 1).ToString();

		mask = Regex.Replace(mask, @"\{autoIncrement\:(\d+)\}", match => next.PadLeft(int.Parse(match!.Groups[1].Value), '0'));
		mask = mask.Replace("{autoIncrement}", next);
		mask = mask.Replace("{title}", title ?? "");
		mask = mask.Replace("{year}", now.Year.ToString());
		mask = mask.Replace("{month}", now.Month.ToString().PadLeft(2, '0'));
		mask = mask.Replace("{day}", now.Day.ToString().PadLeft(2, '0'));
		mask = mask.Replace("{hour}", now.Hour.ToString().PadLeft(2, '0'));
		mask = mask.Replace("{minute}", now.Minute.ToString().PadLeft(2, '0'));
		mask = mask.Replace("{second}", now.Second.ToString().PadLeft(2, '0'));

		return mask;
	}
}
