﻿namespace DKX.Migrations;

public sealed record MigrationResult
{
	public bool Aborted { get; init; }

	public int MigrationsCount { get; init; }
}
