﻿using System.Collections.Generic;

namespace DKX.Migrations;

public sealed record MigrateOptions
{
	public bool? DryRun { get; init; }

	public IReadOnlyCollection<string>? Groups { get; init; }

	public bool? Verbose { get; init; }

	public string? ToVersion { get; init; }

	public bool? Yes { get; init; }
}
