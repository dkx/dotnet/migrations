﻿using System;
using System.Collections.Generic;

namespace DKX.Migrations.Migrations;

public sealed record MigrationsPlan
{
	public Direction Direction { get; init; } = Direction.Up;

	public IReadOnlyCollection<IMigration> Migrations { get; init; } = Array.Empty<IMigration>();
}
