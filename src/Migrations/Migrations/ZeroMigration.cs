﻿namespace DKX.Migrations.Migrations;

public sealed record ZeroMigration : IMigration
{
	public static readonly ZeroMigration Instance = new();

	public string Group => "internal";

	public string Version => "0";

	public string GetContent()
	{
		return string.Empty;
	}
}
