namespace DKX.Migrations.Migrations;

public interface IMigration
{
	string Group { get; }

	string Version { get; }

	string GetContent();
}
