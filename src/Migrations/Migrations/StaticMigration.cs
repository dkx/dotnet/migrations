namespace DKX.Migrations.Migrations;

public sealed record StaticMigration : IMigration
{
	public required string Group { get; init; }

	public required string Version { get; init; }

	public required string Content { get; init; }

	public string GetContent()
	{
		return Content;
	}
}
