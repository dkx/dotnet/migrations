using System.IO;
using System.Text;

namespace DKX.Migrations.Migrations;

public sealed record FileMigration : IMigration
{
	public required string Group { get; init; }

	public required string Version { get; init; }

	public required string Path { get; init; }

	public string GetContent()
	{
		return File.ReadAllText(Path, Encoding.UTF8);
	}
}
