using System;
using DKX.Migrations.Migrations;

namespace DKX.Migrations.Events;

public sealed class BeforeMigrationEventArgs : EventArgs
{
	public BeforeMigrationEventArgs(IMigration migration)
	{
		Migration = migration;
	}

	public IMigration Migration { get; }
}
