using System;
using DKX.Migrations.Migrations;

namespace DKX.Migrations.Events;

public sealed class AfterMigrationEventArgs : EventArgs
{
	public AfterMigrationEventArgs(IMigration migration, TimeSpan elapsed, int affectedRows)
	{
		Migration = migration;
		Elapsed = elapsed;
		AffectedRows = affectedRows;
	}

	public IMigration Migration { get; }

	public TimeSpan Elapsed { get; }

	public int AffectedRows { get; }
}
