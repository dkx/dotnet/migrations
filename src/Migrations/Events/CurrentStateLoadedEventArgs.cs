using System;
using DKX.Migrations.Migrations;

namespace DKX.Migrations.Events;

public sealed class CurrentStateLoadedEventArgs : EventArgs
{
	public CurrentStateLoadedEventArgs(MigrateOptions options, IMigration? lastMigration, MigrationsPlan plan)
	{
		Options = options;
		LastMigration = lastMigration;
		Plan = plan;
	}

	public MigrateOptions Options { get; }

	public IMigration? LastMigration { get; }

	public MigrationsPlan Plan { get; }

	public bool Abort { get; set; }
}
