using System;

namespace DKX.Migrations.Events;

public sealed class BeforeQueryEventArgs : EventArgs
{
	public BeforeQueryEventArgs(string query)
	{
		Query = query;
	}

	public string Query { get; }
}
