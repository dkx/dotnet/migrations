using System;
using System.IO;
using System.Linq;
using System.Text;
using DKX.Migrations.Configurations;
using DKX.Migrations.Loaders;
using DKX.Migrations.Migrations;
using DKX.Migrations.Output;
using DKX.Migrations.Parsing;
using DKX.Migrations.Platforms;
using DKX.Migrations.Utils;
using DKX.Migrations.Watch;

namespace DKX.Migrations;

public sealed class Runner : IDisposable
{
	private readonly Configuration _configuration;

	private readonly IOutputWriter _output;

	private readonly IPlatform _platform;

	private readonly IMigrationsLoader _migrationsLoader;

	private readonly IWatchFactory _watchFactory;

	private readonly Migrator _migrator;

	public Runner(Configuration configuration, IPlatform platform, IOutputWriter output, IWatchFactory watchFactory)
	{
		_configuration = configuration;
		_output = output;
		_platform = platform;
		_migrationsLoader = new RecursiveDirectoriesMigrationsLoader("*.sql", configuration.Groups);
		_watchFactory = watchFactory;
		_migrator = new Migrator(_platform, _configuration, _migrationsLoader, output, new Parser(), watchFactory);

		_migrator.CurrentStateLoaded += (_, args) =>
		{
			var max = args.Plan.Migrations.LastOrDefault();
			var fromName = args.LastMigration is null ? "0" : $"{args.LastMigration.Group}/{args.LastMigration.Version}";
			var directionTitle = args.Plan.Direction == Direction.Up ? "up" : "down";

			if (max is null)
			{
				_output.WriteLine("No migrations to execute");
			}
			else
			{
				if (args.Options.DryRun == true)
				{
					_output.WriteLine($"Executing dry run of migration {directionTitle} to {max.Group}/{max.Version} from {fromName}");
				}
				else
				{
					if (args.Options.Yes != true)
					{
						_output.WriteLine(
							"WARNING! You are about to execute a database migration that could result in schema changes and data loss. Are you sure you wish to continue? (yes/no)"
						);
						var answer = _output.ReadLine();
						if (answer?.Equals("yes", StringComparison.CurrentCultureIgnoreCase) != true)
						{
							_output.WriteLine("Migration aborted");
							args.Abort = true;
							return;
						}
					}

					_output.WriteLine($"Migrating {directionTitle} to {max.Group}/{max.Version} from {fromName}");
				}
			}

			_output.WriteLine();
		};

		_migrator.BeforeMigration += (_, args) =>
		{
			_output.WriteLine($"++ migrating {args.Migration.Group}/{args.Migration.Version}");
		};

		_migrator.NoQueries += (_, _) =>
		{
			_output.WriteLine("  -> WARNING! No queries to execute");
		};

		_migrator.BeforeQuery += (_, args) =>
		{
			_output.WriteLine($"  -> {args.Query.Replace("\r", "").Replace("\n", "\n     ")}");
		};

		_migrator.AfterMigration += (_, args) =>
		{
			_output.WriteLine($"++ migrated (took {args.Elapsed.TotalMilliseconds}ms, {args.AffectedRows} rows affected)");
			_output.WriteLine();
		};
	}

	public Runner(Configuration configuration, IOutputWriter output, IWatchFactory watchFactory)
		: this(configuration, PlatformFactory.Create(configuration.Driver, configuration.ConnectionString), output, watchFactory) { }

	public void Dispose()
	{
		_migrator.Dispose();
	}

	public void Status()
	{
		var currentMigration = _migrator.FindLastExecutedMigration(null);
		var plan = _migrator.GetNextMigrations(null);
		var nextMigration = plan.Migrations.FirstOrDefault();
		var lastMigration = plan.Migrations.LastOrDefault();

		_output.WriteLine("== Configuration");
		_output.WriteLine($"  >> Driver:                                {_configuration.Driver.ToString()}");
		_output.WriteLine($"  >> Platform:                              {_platform.GetType().FullName}");
		_output.WriteLine($"  >> Storage table name:                    {_configuration.StorageTableName}");
		_output.WriteLine($"  >> Storage ID column name:                {_configuration.StorageIdColumnName}");
		_output.WriteLine($"  >> Storage version column name:           {_configuration.StorageVersionColumnName}");
		_output.WriteLine($"  >> Storage executed time column name:     {_configuration.StorageExecutedAtColumnName}");
		_output.WriteLine($"  >> All or nothing:                        {_configuration.AllOrNothing.ToString().ToLower()}");
		_output.WriteLine("  >> Groups:");

		foreach (var group in _configuration.Groups)
		{
			_output.WriteLine($"    >> {group.Name} (" + (group.Enabled ? "enabled" : "disabled") + "): " + group.Directory);
		}

		_output.WriteLine(
			"  >> Current version:                       "
				+ (currentMigration is null ? "0" : $"{currentMigration.Group}/{currentMigration.Version}")
		);
		_output.WriteLine(
			"  >> Next version:                          "
				+ (nextMigration is null ? "0" : $"{nextMigration.Group}/{nextMigration.Version}")
		);
		_output.WriteLine(
			"  >> Last version:                          "
				+ (lastMigration is null ? "0" : $"{lastMigration.Group}/{lastMigration.Version}")
		);

		if (plan.Migrations.Count > 0)
		{
			_output.WriteLine();
			_output.WriteLine("== Available Migration Versions");

			foreach (var migration in plan.Migrations)
			{
				_output.WriteLine($"  >> {migration.Group}/{migration.Version}");
			}
		}
	}

	public void Migrate(MigrateOptions? options = null)
	{
		var opts = options ?? new MigrateOptions();

		using var watch = _watchFactory.Create();
		var result = _migrator.Migrate(opts);

		if (result.Aborted)
		{
			return;
		}

		_output.WriteLine("------------------------");
		_output.WriteLine();
		_output.WriteLine($"++ finished in {watch.Elapsed.TotalMilliseconds}ms");
		_output.WriteLine($"++ {result.MigrationsCount} migrations executed");
	}

	public void Generate(string group, string? title)
	{
		var groupDefinition = _configuration.Groups.FirstOrDefault(g => g.Name == group);
		if (groupDefinition is null)
		{
			throw new ArgumentException($"Unknown group {group}");
		}

		var migrations = _migrationsLoader.LoadMigrations();
		var name = MigrationGenerator.Generate(_configuration.GeneratedStructure, migrations.Count, DateTime.Now, title);
		var fullPath = Path.Join(groupDefinition.Directory, name + ".sql");

		if (Path.GetDirectoryName(fullPath) is not { } directory)
		{
			throw new InvalidOperationException($"Invalid path {fullPath}");
		}

		Directory.CreateDirectory(directory);
		File.WriteAllText(
			fullPath,
			"""
			-- # Migration Up

			-- # Migration Down
			""",
			Encoding.UTF8
		);

		_output.WriteLine($"Generated new migration to \"{fullPath}\"");
	}
}
