using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using DKX.Migrations.Platforms;
using Json.Schema;

namespace DKX.Migrations.Configurations;

public static class FileConfigurationFactory
{
	private const string SchemaName = "config-03.json";

	public static async Task<Configuration> Create(string path)
	{
		var schema = await FetchSchema();
		var data = await File.ReadAllTextAsync(path, Encoding.UTF8);
		var json = JsonDocument.Parse(data).RootElement;

		var result = schema.Evaluate(
			json,
			new EvaluationOptions
			{
				EvaluateAs = SpecVersion.Draft7,
				ValidateAgainstMetaSchema = true,
				OutputFormat = OutputFormat.List,
			}
		);

		if (result.HasErrors)
		{
			throw new Exception(
				"Config error:" + Environment.NewLine + string.Join(Environment.NewLine, result.Errors!.Select(e => $"{e.Key}: {e.Value}"))
			);
		}

		var dir = Path.GetDirectoryName(path);
		if (dir is null)
		{
			throw new Exception("Invalid config path " + path);
		}

		var groups = new List<IGroupConfiguration>();

		foreach (var group in json.GetProperty("groups").EnumerateObject())
		{
			// ReSharper disable once SimplifyConditionalTernaryExpression
			var enabled = group.Value.TryGetProperty("enabled", out var setEnabled) ? setEnabled.GetBoolean() : true;

			groups.Add(
				new GroupConfiguration
				{
					Name = group.Name,
					Directory = Path.GetFullPath(Path.Combine(dir, group.Value.GetProperty("path").GetString()!)),
					Enabled = enabled,
				}
			);
		}

		var connection = json.GetProperty("connection");
		var connectionString = connection.GetProperty("string").GetString()!;
		var driverName = connection.GetProperty("driver").GetString()!;

		var driver = driverName switch
		{
			"sqlite" => Driver.Sqlite,
			"mysql" => Driver.MySql,
			"pgsql" => Driver.Pgsql,
			"mssql" => Driver.MsSql,
			_ => throw new ArgumentException($"Driver '{driverName}' is not supported"),
		};

		var config = new Configuration
		{
			Driver = driver,
			ConnectionString = connectionString,
			Groups = groups,
		};

		if (json.TryGetProperty("storage", out var storage))
		{
			if (storage.TryGetProperty("tableName", out var tableName))
			{
				config = config with { StorageTableName = tableName.GetString()! };
			}

			if (storage.TryGetProperty("idColumnName", out var idColumnName))
			{
				config = config with { StorageIdColumnName = idColumnName.GetString()! };
			}

			if (storage.TryGetProperty("versionColumnName", out var versionColumnName))
			{
				config = config with { StorageVersionColumnName = versionColumnName.GetString()! };
			}

			if (storage.TryGetProperty("executedAtColumnName", out var executedAtColumnName))
			{
				config = config with { StorageExecutedAtColumnName = executedAtColumnName.GetString()! };
			}
		}

		if (json.TryGetProperty("allOrNothing", out var allOrNothing))
		{
			config = config with { AllOrNothing = allOrNothing.GetBoolean() };
		}

		if (json.TryGetProperty("generatedStructure", out var generatedStructure))
		{
			config = config with { GeneratedStructure = generatedStructure.GetString()! };
		}

		return config;
	}

	private static async Task<JsonSchema> FetchSchema()
	{
		var assembly = typeof(FileConfigurationFactory).Assembly;
		await using var stream = assembly.GetManifestResourceStream($"DKX.Migrations.{SchemaName}")!;
		using var reader = new StreamReader(stream);
		var data = await reader.ReadToEndAsync();

		return JsonSchema.FromText(data);
	}
}
