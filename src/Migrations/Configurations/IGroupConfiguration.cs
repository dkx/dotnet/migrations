namespace DKX.Migrations.Configurations;

public interface IGroupConfiguration
{
	string Name { get; }

	string Directory { get; }

	bool Enabled { get; }
}
