namespace DKX.Migrations.Configurations;

public sealed record GroupConfiguration : IGroupConfiguration
{
	public required string Name { get; init; }

	public required string Directory { get; init; }

	public bool Enabled { get; init; } = true;
}
