using System;
using System.Collections.Generic;
using DKX.Migrations.Platforms;

namespace DKX.Migrations.Configurations;

public sealed record Configuration
{
	public required Driver Driver { get; init; }

	public required string ConnectionString { get; init; }

	public IReadOnlyCollection<IGroupConfiguration> Groups { get; init; } = Array.Empty<IGroupConfiguration>();

	public string StorageTableName { get; init; } = "__dkx_migrations";

	public string StorageIdColumnName { get; init; } = "id";

	public string StorageVersionColumnName { get; init; } = "version";

	public string StorageExecutedAtColumnName { get; init; } = "executed_at";

	public bool AllOrNothing { get; init; } = true;

	public string GeneratedStructure { get; init; } = "{year}/{month}/{year}-{month}-{day}_{hour}-{minute}-{second}_{title}";
}
