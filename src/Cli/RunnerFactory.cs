﻿using System;
using System.Threading.Tasks;
using DKX.Migrations.Configurations;
using DKX.Migrations.Output;
using DKX.Migrations.Watch;

namespace DKX.Migrations.Cli;

internal static class RunnerFactory
{
	public static async Task<Runner> Create(CommonParameters parameters)
	{
		var config = await FileConfigurationFactory.Create(parameters.GetExistingFullPath());

		if (Environment.GetEnvironmentVariable("MIGRATIONS_CONNECTION_STRING") is { } overrideConnectionString)
		{
			config = config with { ConnectionString = overrideConnectionString };
		}

		var output = new ConsoleWriter();
		var watchFactory = new SystemWatchFactory();

		return new Runner(config, output, watchFactory);
	}
}
