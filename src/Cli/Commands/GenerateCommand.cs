using System.Threading.Tasks;
using Cocona;

namespace DKX.Migrations.Cli.Commands;

internal sealed class GenerateCommand
{
	[Command("generate")]
	public async Task Run(CommonParameters parameters, [Argument] string group, [Argument] string? title)
	{
		using var runner = await RunnerFactory.Create(parameters);
		runner.Generate(group, title);
	}
}
