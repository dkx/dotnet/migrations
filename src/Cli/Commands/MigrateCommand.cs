using System;
using System.Threading.Tasks;
using Cocona;

namespace DKX.Migrations.Cli.Commands;

internal sealed class MigrateCommand
{
	[Command("migrate")]
	public async Task Run(
		CommonParameters parameters,
		[Option("group")] string[]? groups,
		[Option] string? to,
		[Option] bool? dryRun,
		[Option] bool? verbose,
		[Option] bool? yes
	)
	{
		var options = new MigrateOptions
		{
			DryRun = dryRun,
			Groups = groups,
			Verbose = verbose,
			ToVersion = to,
			Yes = yes,
		};

		if (string.Equals("true", Environment.GetEnvironmentVariable("DRY_RUN"), StringComparison.OrdinalIgnoreCase))
		{
			options = options with { DryRun = true };
		}

		if (string.Equals("true", Environment.GetEnvironmentVariable("YES"), StringComparison.CurrentCultureIgnoreCase))
		{
			options = options with { Yes = true };
		}

		if (string.Equals("true", Environment.GetEnvironmentVariable("VERBOSE"), StringComparison.CurrentCultureIgnoreCase))
		{
			options = options with { Verbose = true };
		}

		using var runner = await RunnerFactory.Create(parameters);
		runner.Migrate(options);
	}
}
