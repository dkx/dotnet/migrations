using System;
using System.IO;
using System.Threading.Tasks;
using Cocona;
using Cocona.Application;

namespace DKX.Migrations.Cli.Commands;

internal sealed class InitCommand
{
	[Command("init")]
	public async Task Run([FromService] ICoconaAppContextAccessor contextAccessor, CommonParameters parameters)
	{
		var ct = contextAccessor.Current!.CancellationToken;
		var path = parameters.GetFullConfigPath();

		await File.WriteAllTextAsync(
			path,
			"""
			{
				"$schema": "https://gitlab.com/dkx/dotnet/migrations/-/raw/master/schema/config-03.json",
				"connection": {
					"driver": "sqlite",
					"string": "Data Source=:memory:"
				},
				"allOrNothing": true,
				"generatedStructure": "{year}/{month}/{year}-{month}-{day}_{hour}-{minute}-{second}_{title}",
				"storage": {
					"tableName": "__dkx_migrations",
					"idColumnName": "id",
					"versionColumnName": "version",
					"executedAtColumnName": "executed_at"
				},
				"groups": {
					"structure": {
						"path": "./Data/Migrations/structure"
					},
					"basic-data": {
						"path": "./Data/Migrations/basic-data"
					},
					"dummy-data": {
						"path": "./Data/Migrations/dummy-data",
						"enabled": false
					}
				}
			}
			""",
			ct
		);

		Console.WriteLine("Created configuration file " + path);
	}
}
