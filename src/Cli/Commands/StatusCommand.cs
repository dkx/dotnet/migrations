using System.Threading.Tasks;
using Cocona;

namespace DKX.Migrations.Cli.Commands;

internal sealed class StatusCommand
{
	[Command("status")]
	public async Task Run(CommonParameters parameters)
	{
		using var runner = await RunnerFactory.Create(parameters);
		runner.Status();
	}
}
