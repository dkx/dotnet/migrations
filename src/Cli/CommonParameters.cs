﻿using System;
using System.IO;
using Cocona;

namespace DKX.Migrations.Cli;

internal sealed class CommonParameters : ICommandParameterSet
{
	[Option("config")]
	[HasDefaultValue]
	public string ConfigPath { get; set; } = "./migrations.json";

	public string GetFullConfigPath()
	{
		return Path.GetFullPath(ConfigPath);
	}

	public string GetExistingFullPath()
	{
		var path = GetFullConfigPath();
		if (!File.Exists(path))
		{
			throw new ArgumentException($"Configuration file {path} does not exists");
		}

		return path;
	}
}
