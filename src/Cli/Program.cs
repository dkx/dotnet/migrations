﻿using Cocona;
using DKX.Migrations.Cli.Commands;

var builder = CoconaApp.CreateBuilder(
	null,
	options =>
	{
		options.TreatPublicMethodsAsCommands = false;
		options.EnableShellCompletionSupport = true;
	}
);

using var app = builder.Build();

app.AddCommands<GenerateCommand>();
app.AddCommands<InitCommand>();
app.AddCommands<MigrateCommand>();
app.AddCommands<StatusCommand>();

app.Run();
