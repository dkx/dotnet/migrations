FROM mcr.microsoft.com/dotnet/sdk:9.0 AS build

WORKDIR /app
COPY . /app

RUN dotnet publish src/Cli/Cli.csproj \
	-o out \
	-r linux-x64 \
	-c Release \
	--self-contained true \
	-p:PackAsTool=false \
	-p:PublishSingleFile=true \
	-p:CrossGenDuringPublish=false

FROM mcr.microsoft.com/dotnet/runtime-deps:9.0

WORKDIR /app
COPY --from=build /app/out /migrations

RUN ln -s /migrations/DKX.Migrations.Cli /usr/bin/migrations

CMD ["migrations", "status"]
