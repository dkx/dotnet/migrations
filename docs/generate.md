# Migration generating

```bash
$ dotnet migrations generate [groupName] [Migration title]
```

The default mask for generating new .sql files looks like this:

`{year}/{month}/{year}-{month}-{day}_{hour}-{minute}-{second}_{title}`

Mask can be customized with configuration.

**Replacements:**

* `year`: Current year
* `month`: Current month
* `day`: Current day
* `hour`: Current hour
* `minute`: Current minute
* `second`: Current second
* `title`: Title given in CLI generate command
* `autoIncrement`: Auto incrementing number, starting with 1
* `autoIncrement:{serialLength}`: Auto incrementing number, starting with 1 and padded to desired length (`serialLength`)
