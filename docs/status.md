# Status

```bash
$ dotnet migrations status
```

Output:

```
== Configuration
  >> Driver:                                Sqlite
  >> Platform:                              DKX.Migrations.Platforms.SqlitePlatform
  >> Storage table name:                    __dkx_migrations
  >> Storage ID column name:                id
  >> Storage version column name:           version
  >> Storage executed time column name:     executed_at
  >> Groups:
    >> default (enabled): Data/Migrations
  >> Current version:                       0
  >> Next version:                          default/2020-03-25_21-30-15_Init
  >> Last version:                          default/2020-04-01_15-00-30_AddUsers

== Available Migration Versions
  >> default/2020-03-25_21-30-15_Init
  >> default/2020-04-01_15-00-30_AddUsers
```
