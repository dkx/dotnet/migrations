# Docker

**`registry.gitlab.com/dkx/dotnet/migrations`**

```bash
$ docker run --rm -v `pwd`/migrations.json:/app/migrations.json registry.gitlab.com/dkx/dotnet/migrations:<VERSION> migrations migrate
```
