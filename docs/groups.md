# Groups

Groups can be used to mark some migrations as "dev-only" or just to organize them better.

Nice setup would be with 3 groups:

* `structure`:
    * enabled by default
    * changes structure of database - add/alter/delete/... tables, etc.
* `basic-data`:
    * enabled by default
    * inserts default data into database, eg. locations, languages, currency codes, etc.
* `dummy-data`:
    * disabled by default
    * inserts testing or dev only data
    
You can of course define your own groups or keep on using just one.

## Configuration

Groups are defined in your `migrations.json` file.

```json
{
    "$schema": "https://gitlab.com/dkx/dotnet/migrations/-/raw/master/schema/config-02.json",
    "groups": {
        "structure": {
            "path": "./Data/Migrations/structure"
        },
        "basic-data": {
            "path": "./Data/Migrations/basic-data"
        },
        "dummy-data": {
            "path": "./Data/Migrations/dummy-data",
            "enabled": false
        }
    }
}
```

Now only migrations within the `structure` and `basic-data` will be executed when running the `dotnet migrations migrate`
command.

## Running disabled groups

Active groups can be overriden with CLI option `--group`. If you use this option, you must list all groups you want to 
migrate.

```bash
$ dotnet migrations migrate --group structure --group basic-data --group dummy-data
```
