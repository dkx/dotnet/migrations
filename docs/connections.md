# Connections

Supported connections:

* Sqlite (`Microsoft.Data.Sqlite`)
* MySql (`MySql.Data`)
* Postgres (`Npgsql`)
* MsSql (`System.Data.SqlClient`)

Internally, this library is written for ADO.NET connections, so adding support for other implementations is not a 
difficult task.
