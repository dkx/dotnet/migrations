## Dry run

```bash
$ dotnet migrations migrate --dry-run
```

The output of this command is almost same as without the `--dry-run` argument.

Difference is that no migration will be actually executed.

This option can be enabled by `DRY_RUN=true` environment variable too.
