# Configuration

Default config created with init command:

```bash
$ dotnet migrations init
```

```json
{
    "$schema": "https://gitlab.com/dkx/dotnet/migrations/-/raw/master/schema/config-03.json",
    "connection": {
        "driver": "sqlite",
        "string": "Data Source=:memory:"
    },
    "allOrNothing": true,
    "generatedStructure": "{year}/{month}/{year}-{month}-{day}_{hour}-{minute}-{second}_{title}",
    "storage": {
        "tableName": "__dkx_migrations",
        "idColumnName": "id",
        "versionColumnName": "version",
        "executedAtColumnName": "executed_at"
    },
    "groups": {
        "structure": {
            "path": "./Data/Migrations/structure"
        },
        "basic-data": {
            "path": "./Data/Migrations/basic-data"
        },
        "dummy-data": {
            "path": "./Data/Migrations/dummy-data",
            "enabled": false
        }
    }
}
```

## Connection string

Connection string configured in `migrations.json` should not contain any production sensitive data and should be used
only for local development.

Default connection string can be overwritten with environment variable `MIGRATIONS_CONNECTION_STRING`.

## All or nothing

This option controls how to wrap your queries in transactions.

* When set to `true` all new migration files will run in one transaction. When one file fails, all will rollback.
* When set to `false` each file will run in its own transaction. When one file fail, previous migrations will not be affected.

## Generated structure

See [migration generating](./generate.md)
