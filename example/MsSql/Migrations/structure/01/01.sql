-- # Migration Up
CREATE TABLE dbo.users (id INT IDENTITY (1, 1) PRIMARY KEY, email VARCHAR(255) NOT NULL);

-- # Migration Down
DROP TABLE dbo.users;
