-- # Migration Up
INSERT INTO users (email) VALUES ('lord@voldemort.com');

-- # Migration Down
DELETE FROM users WHERE email = 'lord@voldemort.com';
