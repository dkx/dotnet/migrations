-- # Migration Up
CREATE TABLE users (
	id INTEGER PRIMARY KEY,
	email TEXT NOT NULL
);

-- # Migration Down
DROP TABLE users;
