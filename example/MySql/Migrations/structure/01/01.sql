-- # Migration Up
CREATE TABLE users (id INTEGER NOT NULL AUTO_INCREMENT, email VARCHAR(255) NOT NULL, PRIMARY KEY (id)) ENGINE=InnoDB;

-- # Migration Down
DROP TABLE users;
