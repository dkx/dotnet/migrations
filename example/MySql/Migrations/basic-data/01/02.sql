-- # Migration Up
INSERT INTO users (email) VALUES ('john@doe.com');
INSERT INTO users (email) VALUES ('joanne@rowling.com');

-- # Migration Down
DELETE FROM users WHERE email = 'joanne@rowling.com';
DELETE FROM users WHERE email = 'john@doe.com';
