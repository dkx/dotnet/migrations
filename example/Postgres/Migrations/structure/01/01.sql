-- # Migration Up
CREATE TABLE users (id SERIAL, email VARCHAR NOT NULL);

-- # Migration Down
DROP TABLE users;
